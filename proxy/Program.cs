﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using proxy.Properties;

namespace proxy
{
    // ReSharper disable once ArrangeTypeModifiers
    class Program
    {
        static Mutex _mutex;

        [STAThread]
        static void Main(string[] args)
        {
            for (int i = 0; i != args.Length; ++i)
            {
                if (args[i].Split('=')[0].ToLower() == "/u")
                {
                    string guid = args[i].Split('=')[1];
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.System);
                    var uninstallProcess = new ProcessStartInfo(path + "\\msiexec.exe", "/x " + guid);
                    Process.Start(uninstallProcess);
                    return;
                }
            }



            bool alone = true;
            _mutex = new Mutex(false, "MimicShell", out alone);
            //    SetAccessRule(Helpers.APP_FOLDER_PATH);
            if (alone)
            {
                ProgramManager.Instance.OnStartApp(args);
            }
            else
            {
                MessageBox.Show(
                   Resources.gl_AppAlreadyStarted,
                   Resources.gl_Txt_Message,
                   MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //public static void SetAccessRule(string directory)
        //{
        //    System.Security.AccessControl.DirectorySecurity sec = System.IO.Directory.GetAccessControl(directory);
        //    Logger.LogWriter.Instance.WriteToLog(sec.AccessRightType.ToString());
        //    //  FileSystemAccessRule accRule = new FileSystemAccessRule(Environment.UserDomainName + "\\" + Environment.UserName, FileSystemRights.FullControl, AccessControlType.Allow);
        //    FileSystemAccessRule accRule = new FileSystemAccessRule(directory, FileSystemRights.FullControl, AccessControlType.Allow);
        //    sec.AddAccessRule(accRule);
        //    Logger.LogWriter.Instance.WriteToLog(sec.AccessRightType.ToString());
        //}

    }
}

