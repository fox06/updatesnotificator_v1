﻿namespace proxy
{
    public enum UpdatesStatus
    {
        InProgress,
        None,
        IsCompleted,
        DatabaseLoading,
        DatabaseSaving,
        AddiditionInfo,
        Error
    }
}