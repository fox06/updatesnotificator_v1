﻿using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using proxy.Logger;

namespace proxy
{
    class EmailSender
    {
        private MailMessage _message;
        private readonly SmtpClient Smtp;
        private readonly string from;

        public EmailSender(string smtpServer, int smtpPort, string from, string pass, string login, bool enableSsl = false)
        {
            Smtp = new SmtpClient(smtpServer, smtpPort);
            Smtp.Credentials = new NetworkCredential(login, pass);
            Smtp.EnableSsl = enableSsl;
            this.from = from;

        }

        public void SendMail(string subject, string bodytext, string recivers)
        {
            _message = new MailMessage();

            _message.From = new MailAddress(from);
            _message.Subject = subject;
            _message.Body = bodytext;

            SetRecivers(recivers);

            if (_message.To.Count == 0)
            {
                LogWriter.Instance.WriteToLog("Список получателей рассылки уведомлений пуст", LogLevel.Warning);
                return;
            }

            Smtp.Send(_message);
        }


        private void AttachFile(string filepath)
        {
            //Прикрепляем файл
            Attachment attach = new Attachment(filepath, MediaTypeNames.Application.Octet);

            // Добавляем информацию для файла
            ContentDisposition disposition = attach.ContentDisposition;
            disposition.CreationDate = File.GetCreationTime(filepath);
            disposition.ModificationDate = File.GetLastWriteTime(filepath);
            disposition.ReadDate = File.GetLastAccessTime(filepath);

            _message.Attachments.Add(attach);
        }

        private void SetRecivers(string[] recivers)
        {
            for (int i = 0; i < recivers.Length - 1; i++)
            {
                _message.To.Add(new MailAddress(recivers[i]));
            }

        }

        private void SetRecivers(string emails)
        {

            string[] recivers = ExtractEmails(emails);

            for (int i = 0; i < recivers.Length; i++)
            {
                _message.To.Add(new MailAddress(recivers[i]));
            }

        }

        private string[] ExtractEmails(string str)
        {

            string RegexPattern = @"\b[A-Z0-9._-]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z.]{2,6}\b";

            // Find matches
            MatchCollection matches
                = Regex.Matches(str, RegexPattern, RegexOptions.IgnoreCase);

            string[] MatchList = new string[matches.Count];

            // add each match
            int c = 0;
            foreach (Match match in matches)
            {
                MatchList[c] = match.ToString();
                c++;
            }

            return MatchList;
        }


    }
}

