namespace proxy.Logger
{
    public enum LogLevel
    {
        Info,
        Warning,
        Error,
        Critical
    }
}