﻿using System;
using System.Data;
using System.Windows.Forms;

namespace proxy.Forms
{
    public partial class ProductsSelectionForm : Form
    {
        
        public ProductsSelectionForm()
        {
            InitializeComponent();            
        }

        private void ProductsSelection_Load(object sender, EventArgs e)
        {
            Application.UseWaitCursor = false;

            DataView dv = new DataView(DataBase.Instance.Products);
            dv.Sort = "Name ASC";
            dataGridView1.DataSource = dv;

            // dataGridView1.DataBindings = DataBase.Instance.Products;


            dataGridView1.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridView1.Columns[1].MinimumWidth = 100;
            // dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[2].Visible = false;
            dataGridView1.Columns[3].Visible = false;
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[7].Visible = false;
            dataGridView1.Columns[8].Visible = false;
            dataGridView1.Columns[9].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Наименование";
            dataGridView1.Columns[1].ReadOnly = true;

            dataGridView1.Columns[11].HeaderText = "Метка";
            
            dataGridView1.Columns[10].HeaderText = "Доступность";
            dataGridView1.Columns[10].ReadOnly = true;
                        
        }        

        private void ConfirmChanges()
        {
            dataGridView1.EndEdit();
            DataBase.Instance.Products.AcceptChanges();           
        }



        private void ProductsSelectionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ConfirmChanges();
        }
      

        private void checkBoxSelectDeselect_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox s = (CheckBox)sender;

            if (s.Checked)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].Cells[11].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].Cells[11].Value = false;
                }
            }
        }

    }
}
