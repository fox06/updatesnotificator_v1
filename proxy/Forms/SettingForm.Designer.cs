﻿using System.ComponentModel;
using System.Windows.Forms;

namespace proxy.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dateTimeTimeTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimeFrom = new System.Windows.Forms.DateTimePicker();
            this.textBoxCheckInterval = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkedListBoxDaysOfWeek = new System.Windows.Forms.CheckedListBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBoxAutostart = new System.Windows.Forms.CheckBox();
            this.checkBoxDefaultUpdFolder = new System.Windows.Forms.CheckBox();
            this.buttonFolderDialog = new System.Windows.Forms.Button();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoUpdFolder = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxEmail = new System.Windows.Forms.CheckBox();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.Controls.Add(this.tabControl1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(436, 409);
            this.panel3.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl1.Location = new System.Drawing.Point(3, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(439, 410);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Tag = "";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(431, 384);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Доступ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dateTimeTimeTo);
            this.groupBox5.Controls.Add(this.dateTimeFrom);
            this.groupBox5.Controls.Add(this.textBoxCheckInterval);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.checkedListBoxDaysOfWeek);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Location = new System.Drawing.Point(3, 214);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(425, 164);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Расписание";
            // 
            // dateTimeTimeTo
            // 
            this.dateTimeTimeTo.CustomFormat = "HH:mm";
            this.dateTimeTimeTo.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::proxy.Properties.Settings.Default, "TimeTo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dateTimeTimeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeTimeTo.Location = new System.Drawing.Point(122, 25);
            this.dateTimeTimeTo.Name = "dateTimeTimeTo";
            this.dateTimeTimeTo.ShowUpDown = true;
            this.dateTimeTimeTo.Size = new System.Drawing.Size(54, 20);
            this.dateTimeTimeTo.TabIndex = 9;
            this.dateTimeTimeTo.Value = global::proxy.Properties.Settings.Default.TimeTo;
            this.dateTimeTimeTo.Validating += new System.ComponentModel.CancelEventHandler(this.maskedTextTime_Validating);
            // 
            // dateTimeFrom
            // 
            this.dateTimeFrom.CustomFormat = "HH:mm";
            this.dateTimeFrom.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::proxy.Properties.Settings.Default, "TimeFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dateTimeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeFrom.Location = new System.Drawing.Point(39, 25);
            this.dateTimeFrom.Name = "dateTimeFrom";
            this.dateTimeFrom.ShowUpDown = true;
            this.dateTimeFrom.Size = new System.Drawing.Size(54, 20);
            this.dateTimeFrom.TabIndex = 8;
            this.dateTimeFrom.Value = global::proxy.Properties.Settings.Default.TimeFrom;
            this.dateTimeFrom.Validating += new System.ComponentModel.CancelEventHandler(this.maskedTextTime_Validating);
            // 
            // textBoxCheckInterval
            // 
            this.textBoxCheckInterval.Location = new System.Drawing.Point(194, 25);
            this.textBoxCheckInterval.Mask = "000";
            this.textBoxCheckInterval.Name = "textBoxCheckInterval";
            this.textBoxCheckInterval.PromptChar = ' ';
            this.textBoxCheckInterval.Size = new System.Drawing.Size(24, 20);
            this.textBoxCheckInterval.TabIndex = 7;
            this.textBoxCheckInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(224, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(180, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Периодичность проверки(минуты)";
            // 
            // checkedListBoxDaysOfWeek
            // 
            this.checkedListBoxDaysOfWeek.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxDaysOfWeek.CheckOnClick = true;
            this.checkedListBoxDaysOfWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBoxDaysOfWeek.FormattingEnabled = true;
            this.checkedListBoxDaysOfWeek.IntegralHeight = false;
            this.checkedListBoxDaysOfWeek.Items.AddRange(new object[] {
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            "Пятница",
            "Суббота",
            "Воскесенье"});
            this.checkedListBoxDaysOfWeek.Location = new System.Drawing.Point(29, 50);
            this.checkedListBoxDaysOfWeek.MultiColumn = true;
            this.checkedListBoxDaysOfWeek.Name = "checkedListBoxDaysOfWeek";
            this.checkedListBoxDaysOfWeek.Size = new System.Drawing.Size(353, 108);
            this.checkedListBoxDaysOfWeek.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(97, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "по";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "С";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBox3);
            this.groupBox4.Controls.Add(this.checkBoxAutostart);
            this.groupBox4.Controls.Add(this.checkBoxDefaultUpdFolder);
            this.groupBox4.Controls.Add(this.buttonFolderDialog);
            this.groupBox4.Controls.Add(this.checkBox5);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.textBoUpdFolder);
            this.groupBox4.Controls.Add(this.checkBox2);
            this.groupBox4.Location = new System.Drawing.Point(3, 92);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(425, 116);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Основные";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = global::proxy.Properties.Settings.Default.NotifyContentUpd;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::proxy.Properties.Settings.Default, "NotifyContentUpd", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox3.Location = new System.Drawing.Point(17, 92);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(247, 17);
            this.checkBox3.TabIndex = 24;
            this.checkBox3.Text = "Отображать внешние файлы конфигураций";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBoxAutostart
            // 
            this.checkBoxAutostart.AutoSize = true;
            this.checkBoxAutostart.Checked = global::proxy.Properties.Settings.Default.Autostart;
            this.checkBoxAutostart.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::proxy.Properties.Settings.Default, "Autostart", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBoxAutostart.Location = new System.Drawing.Point(15, 19);
            this.checkBoxAutostart.Name = "checkBoxAutostart";
            this.checkBoxAutostart.Size = new System.Drawing.Size(96, 17);
            this.checkBoxAutostart.TabIndex = 23;
            this.checkBoxAutostart.Text = "Автозагрузка";
            this.checkBoxAutostart.UseVisualStyleBackColor = true;
            // 
            // checkBoxDefaultUpdFolder
            // 
            this.checkBoxDefaultUpdFolder.AutoSize = true;
            this.checkBoxDefaultUpdFolder.Checked = global::proxy.Properties.Settings.Default.UseDefaultUpdFolder;
            this.checkBoxDefaultUpdFolder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDefaultUpdFolder.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::proxy.Properties.Settings.Default, "UseDefaultUpdFolder", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBoxDefaultUpdFolder.Location = new System.Drawing.Point(17, 69);
            this.checkBoxDefaultUpdFolder.Name = "checkBoxDefaultUpdFolder";
            this.checkBoxDefaultUpdFolder.Size = new System.Drawing.Size(262, 17);
            this.checkBoxDefaultUpdFolder.TabIndex = 22;
            this.checkBoxDefaultUpdFolder.Text = "Использовать локальный подкаталог Updates";
            this.checkBoxDefaultUpdFolder.UseVisualStyleBackColor = true;
            this.checkBoxDefaultUpdFolder.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // buttonFolderDialog
            // 
            this.buttonFolderDialog.Location = new System.Drawing.Point(381, 40);
            this.buttonFolderDialog.Name = "buttonFolderDialog";
            this.buttonFolderDialog.Size = new System.Drawing.Size(23, 23);
            this.buttonFolderDialog.TabIndex = 21;
            this.buttonFolderDialog.Text = "...";
            this.buttonFolderDialog.UseVisualStyleBackColor = true;
            this.buttonFolderDialog.Click += new System.EventHandler(this.buttonFolderDialog_Click);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Checked = global::proxy.Properties.Settings.Default.ToTray;
            this.checkBox5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox5.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::proxy.Properties.Settings.Default, "ToTray", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox5.Location = new System.Drawing.Point(117, 19);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(126, 17);
            this.checkBox5.TabIndex = 20;
            this.checkBox5.Text = "Cворачивать в трей";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Путь загрузки";
            // 
            // textBoUpdFolder
            // 
            this.textBoUpdFolder.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "UpdatesFolder", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoUpdFolder.Location = new System.Drawing.Point(100, 42);
            this.textBoUpdFolder.Name = "textBoUpdFolder";
            this.textBoUpdFolder.ReadOnly = true;
            this.textBoUpdFolder.Size = new System.Drawing.Size(275, 20);
            this.textBoUpdFolder.TabIndex = 16;
            this.textBoUpdFolder.Text = global::proxy.Properties.Settings.Default.UpdatesFolder;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = global::proxy.Properties.Settings.Default.DownloadUpdates;
            this.checkBox2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::proxy.Properties.Settings.Default, "DownloadUpdates", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox2.Location = new System.Drawing.Point(247, 19);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(172, 17);
            this.checkBox2.TabIndex = 0;
            this.checkBox2.Text = "Автоматическое скачивание";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.textBoxPass);
            this.groupBox3.Controls.Add(this.textBox6);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(425, 83);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Авторизация на сайте users.1c.v8.ru";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Пароль";
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(95, 46);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.PasswordChar = '*';
            this.textBoxPass.Size = new System.Drawing.Size(150, 20);
            this.textBoxPass.TabIndex = 14;
            // 
            // textBox6
            // 
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "Login", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox6.Location = new System.Drawing.Point(95, 20);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(150, 20);
            this.textBox6.TabIndex = 12;
            this.textBox6.Text = global::proxy.Properties.Settings.Default.Login;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Пользователь";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.checkBoxEmail);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(431, 384);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Уведомления";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.richTextBox1);
            this.groupBox2.Controls.Add(this.textBoxEmail);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(3, 119);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(422, 139);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Тело";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(220, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Получатели (используйте разделитель \";\"";
            // 
            // richTextBox1
            // 
            this.richTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "EMails", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.richTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.richTextBox1.Enabled = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.richTextBox1.Location = new System.Drawing.Point(6, 69);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(410, 145);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = global::proxy.Properties.Settings.Default.EMails;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "EMailFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxEmail.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxEmail.Enabled = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.textBoxEmail.Location = new System.Drawing.Point(85, 19);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(176, 20);
            this.textBoxEmail.TabIndex = 1;
            this.textBoxEmail.Text = global::proxy.Properties.Settings.Default.EMailFrom;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Отправитель";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(9, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(416, 84);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Авторизация";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = global::proxy.Properties.Settings.Default.EnableSSL;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::proxy.Properties.Settings.Default, "EnableSSL", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox1.Enabled = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.checkBox1.Location = new System.Drawing.Point(320, 21);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(46, 17);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "SSL";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(204, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Pass";
            // 
            // textBox4
            // 
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "EMailPass", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox4.Enabled = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.textBox4.Location = new System.Drawing.Point(236, 49);
            this.textBox4.Name = "textBox4";
            this.textBox4.PasswordChar = '*';
            this.textBox4.Size = new System.Drawing.Size(150, 20);
            this.textBox4.TabIndex = 9;
            this.textBox4.Text = global::proxy.Properties.Settings.Default.EMailPass;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Login";
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "EMailLogin", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox3.Enabled = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.textBox3.Location = new System.Drawing.Point(49, 49);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(150, 20);
            this.textBox3.TabIndex = 7;
            this.textBox3.Text = global::proxy.Properties.Settings.Default.EMailLogin;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Port";
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "Port", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox2.Enabled = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.textBox2.Location = new System.Drawing.Point(236, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(59, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.Text = global::proxy.Properties.Settings.Default.Port;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::proxy.Properties.Settings.Default, "SMTPServer", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox1.Enabled = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.textBox1.Location = new System.Drawing.Point(48, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = global::proxy.Properties.Settings.Default.SMTPServer;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "SMTP";
            // 
            // checkBoxEmail
            // 
            this.checkBoxEmail.AutoSize = true;
            this.checkBoxEmail.Checked = global::proxy.Properties.Settings.Default.UsingEmailNotifying;
            this.checkBoxEmail.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::proxy.Properties.Settings.Default, "UsingEmailNotifying", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBoxEmail.Location = new System.Drawing.Point(16, 6);
            this.checkBoxEmail.Name = "checkBoxEmail";
            this.checkBoxEmail.Size = new System.Drawing.Size(141, 17);
            this.checkBoxEmail.TabIndex = 0;
            this.checkBoxEmail.Text = "Уведомление по e-mail";
            this.checkBoxEmail.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(438, 411);
            this.Controls.Add(this.panel3);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(454, 450);
            this.MinimumSize = new System.Drawing.Size(454, 450);
            this.Name = "SettingsForm";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "Настройки";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.panel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel panel3;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private GroupBox groupBox5;
        private DateTimePicker dateTimeTimeTo;
        private DateTimePicker dateTimeFrom;
        private MaskedTextBox textBoxCheckInterval;
        private Label label12;
        private CheckedListBox checkedListBoxDaysOfWeek;
        private Label label11;
        private Label label9;
        private GroupBox groupBox4;
        private CheckBox checkBoxAutostart;
        private CheckBox checkBoxDefaultUpdFolder;
        private Button buttonFolderDialog;
        private CheckBox checkBox5;
        private Label label7;
        private TextBox textBoUpdFolder;
        private CheckBox checkBox2;
        private GroupBox groupBox3;
        private Label label8;
        private TextBox textBoxPass;
        private TextBox textBox6;
        private Label label10;
        private TabPage tabPage2;
        private GroupBox groupBox2;
        private Label label6;
        private RichTextBox richTextBox1;
        private TextBox textBoxEmail;
        private Label label1;
        private GroupBox groupBox1;
        private CheckBox checkBox1;
        private Label label5;
        private TextBox textBox4;
        private Label label4;
        private TextBox textBox3;
        private Label label3;
        private TextBox textBox2;
        private TextBox textBox1;
        private Label label2;
        private CheckBox checkBoxEmail;
        private CheckBox checkBox3;
       // private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        // private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
    }
}