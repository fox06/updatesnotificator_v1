﻿using System.ComponentModel;
using System.Windows.Forms;
using DataGridViewCustomCell;

namespace proxy.Forms
{
    partial class MiniDownloaderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MiniDownloaderForm));
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.miniDownloaderGridView = new System.Windows.Forms.DataGridView();
            this.NeedDownload = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Discription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Progress = new proxy.DataGridViewProgressColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewProgressColumn1 = new proxy.DataGridViewProgressColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.downloadStatusColumn1 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.buttonOpenFilesFolder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.miniDownloaderGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(417, 143);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Начать загрузку";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(3, 145);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(211, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Выбрать все / отменить выбор всех";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // miniDownloaderGridView
            // 
            this.miniDownloaderGridView.AllowUserToOrderColumns = true;
            this.miniDownloaderGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.miniDownloaderGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.miniDownloaderGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.miniDownloaderGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.miniDownloaderGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.miniDownloaderGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NeedDownload,
            this.Discription,
            this.FileName,
            this.Progress,
            this.Total});
            this.miniDownloaderGridView.Location = new System.Drawing.Point(3, 3);
            this.miniDownloaderGridView.Name = "miniDownloaderGridView";
            this.miniDownloaderGridView.RowHeadersVisible = false;
            this.miniDownloaderGridView.Size = new System.Drawing.Size(520, 134);
            this.miniDownloaderGridView.TabIndex = 3;
            // 
            // NeedDownload
            // 
            this.NeedDownload.FillWeight = 50.76142F;
            this.NeedDownload.HeaderText = "";
            this.NeedDownload.MinimumWidth = 20;
            this.NeedDownload.Name = "NeedDownload";
            this.NeedDownload.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NeedDownload.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Discription
            // 
            this.Discription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Discription.DataPropertyName = "FileDiscription";
            this.Discription.FillWeight = 126.9035F;
            this.Discription.HeaderText = "Описание";
            this.Discription.MinimumWidth = 10;
            this.Discription.Name = "Discription";
            // 
            // FileName
            // 
            this.FileName.DataPropertyName = "FileName";
            this.FileName.FillWeight = 107.445F;
            this.FileName.HeaderText = "Имя файла";
            this.FileName.Name = "FileName";
            // 
            // Progress
            // 
            this.Progress.DataPropertyName = "Progress";
            this.Progress.FillWeight = 107.445F;
            this.Progress.HeaderText = "Состояние";
            this.Progress.Name = "Progress";
            this.Progress.ProgressBarColor = System.Drawing.Color.Green;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "TotalBytes";
            this.Total.FillWeight = 107.445F;
            this.Total.HeaderText = "Размер";
            this.Total.Name = "Total";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "discription";
            this.dataGridViewTextBoxColumn1.FillWeight = 184.7716F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Файл";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 211;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Progress";
            this.dataGridViewTextBoxColumn2.HeaderText = "Состояние";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 141;
            // 
            // dataGridViewProgressColumn1
            // 
            this.dataGridViewProgressColumn1.DataPropertyName = "Progress";
            this.dataGridViewProgressColumn1.HeaderText = "Состояние";
            this.dataGridViewProgressColumn1.Name = "dataGridViewProgressColumn1";
            this.dataGridViewProgressColumn1.ProgressBarColor = System.Drawing.Color.Green;
            this.dataGridViewProgressColumn1.Width = 126;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "TotalBytes";
            this.dataGridViewTextBoxColumn3.HeaderText = "Размер";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 126;
            // 
            // downloadStatusColumn1
            // 
            this.downloadStatusColumn1.DataPropertyName = "Progress";
            this.downloadStatusColumn1.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn1.HeaderText = "Состояние";
            this.downloadStatusColumn1.Name = "downloadStatusColumn1";
            this.downloadStatusColumn1.Width = 141;
            // 
            // buttonOpenFilesFolder
            // 
            this.buttonOpenFilesFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenFilesFolder.Location = new System.Drawing.Point(305, 143);
            this.buttonOpenFilesFolder.Name = "buttonOpenFilesFolder";
            this.buttonOpenFilesFolder.Size = new System.Drawing.Size(106, 23);
            this.buttonOpenFilesFolder.TabIndex = 4;
            this.buttonOpenFilesFolder.Text = "Открыть папку";
            this.buttonOpenFilesFolder.UseVisualStyleBackColor = true;
            this.buttonOpenFilesFolder.Click += new System.EventHandler(this.buttonOpenFilesFolder_Click);
            // 
            // MiniDownloaderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 174);
            this.Controls.Add(this.buttonOpenFilesFolder);
            this.Controls.Add(this.miniDownloaderGridView);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MiniDownloaderForm";
            this.Text = "Загрузка файлов";
            this.Load += new System.EventHandler(this.MiniDownloaderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.miniDownloaderGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button button1;
        private CheckBox checkBox1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DownloadStatusColumn downloadStatusColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridView miniDownloaderGridView;
        private DataGridViewProgressColumn dataGridViewProgressColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewCheckBoxColumn NeedDownload;
        private DataGridViewTextBoxColumn Discription;
        private DataGridViewTextBoxColumn FileName;
        private DataGridViewProgressColumn Progress;
        private DataGridViewTextBoxColumn Total;
        private Button buttonOpenFilesFolder;
    }
}