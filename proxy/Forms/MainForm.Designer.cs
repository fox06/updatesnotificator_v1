﻿using System.ComponentModel;
using System.Windows.Forms;
using DataGridViewCustomCell;

namespace proxy.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выборКонфигурацийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelUp = new System.Windows.Forms.Panel();
            this.UpdateInfoLabelAdd = new System.Windows.Forms.Label();
            this.pictureStatus = new System.Windows.Forms.PictureBox();
            this.StopChekingButton = new System.Windows.Forms.Button();
            this.CheckUpdateButton = new System.Windows.Forms.Button();
            this.UpdateInfoLabel = new System.Windows.Forms.Label();
            this.ConnectionStatusLabel = new System.Windows.Forms.Label();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.LabelConnectionStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureConnStatus = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridViewNotify = new System.Windows.Forms.DataGridView();
            this.contextMenuHistoryTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SelectionID = new System.Windows.Forms.ToolStripMenuItem();
            this.SelectionDay = new System.Windows.Forms.ToolStripMenuItem();
            this.CancleSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.DownloadItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageStatusColumn1 = new DataGridViewCustomCell.ImageStatusColumn();
            this.downloadStatusColumn1 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.imageStatusColumn2 = new DataGridViewCustomCell.ImageStatusColumn();
            this.downloadStatusColumn2 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.imageStatusColumn3 = new DataGridViewCustomCell.ImageStatusColumn();
            this.downloadStatusColumn3 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.imageStatusColumn4 = new DataGridViewCustomCell.ImageStatusColumn();
            this.downloadStatusColumn4 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.imageStatusColumn5 = new DataGridViewCustomCell.ImageStatusColumn();
            this.downloadStatusColumn5 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.imageStatusColumn6 = new DataGridViewCustomCell.ImageStatusColumn();
            this.downloadStatusColumn6 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.imageStatusColumn7 = new DataGridViewCustomCell.ImageStatusColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.downloadStatusColumn7 = new DataGridViewCustomCell.DownloadStatusColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeDisc = new DataGridViewCustomCell.ImageStatusColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Config_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DownloadStatus = new DataGridViewCustomCell.DownloadStatusColumn();
            this.menuStrip1.SuspendLayout();
            this.panelUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStatus)).BeginInit();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureConnStatus)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNotify)).BeginInit();
            this.contextMenuHistoryTable.SuspendLayout();
            this.contextMenuNotify.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.настройкиToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(1, 1);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(630, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // ToolStripMenuItem
            // 
            this.ToolStripMenuItem.Name = "ToolStripMenuItem";
            this.ToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.ToolStripMenuItem.Text = "Выход";
            this.ToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SettingsToolStripMenuItem,
            this.выборКонфигурацийToolStripMenuItem});
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            // 
            // SettingsToolStripMenuItem
            // 
            this.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem";
            this.SettingsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.SettingsToolStripMenuItem.Text = "Настройки программы";
            this.SettingsToolStripMenuItem.Click += new System.EventHandler(this.SettingsToolStripMenuItem_Click);
            // 
            // выборКонфигурацийToolStripMenuItem
            // 
            this.выборКонфигурацийToolStripMenuItem.Name = "выборКонфигурацийToolStripMenuItem";
            this.выборКонфигурацийToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.выборКонфигурацийToolStripMenuItem.Text = "Выбор конфигураций";
            this.выборКонфигурацийToolStripMenuItem.Click += new System.EventHandler(this.выборКонфигурацийToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // panelUp
            // 
            this.panelUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelUp.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panelUp.Controls.Add(this.UpdateInfoLabelAdd);
            this.panelUp.Controls.Add(this.pictureStatus);
            this.panelUp.Controls.Add(this.StopChekingButton);
            this.panelUp.Controls.Add(this.CheckUpdateButton);
            this.panelUp.Controls.Add(this.UpdateInfoLabel);
            this.panelUp.Controls.Add(this.ConnectionStatusLabel);
            this.panelUp.Location = new System.Drawing.Point(1, 25);
            this.panelUp.Name = "panelUp";
            this.panelUp.Size = new System.Drawing.Size(630, 47);
            this.panelUp.TabIndex = 1;
            // 
            // UpdateInfoLabelAdd
            // 
            this.UpdateInfoLabelAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdateInfoLabelAdd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UpdateInfoLabelAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.UpdateInfoLabelAdd.Location = new System.Drawing.Point(171, 25);
            this.UpdateInfoLabelAdd.Name = "UpdateInfoLabelAdd";
            this.UpdateInfoLabelAdd.Size = new System.Drawing.Size(417, 18);
            this.UpdateInfoLabelAdd.TabIndex = 7;
            this.UpdateInfoLabelAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureStatus
            // 
            this.pictureStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureStatus.ErrorImage = null;
            this.pictureStatus.Image = global::proxy.Properties.Resources.onebit_06;
            this.pictureStatus.Location = new System.Drawing.Point(594, 9);
            this.pictureStatus.Name = "pictureStatus";
            this.pictureStatus.Size = new System.Drawing.Size(30, 28);
            this.pictureStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureStatus.TabIndex = 6;
            this.pictureStatus.TabStop = false;
            // 
            // StopChekingButton
            // 
            this.StopChekingButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.StopChekingButton.Image = global::proxy.Properties.Resources.Stop;
            this.StopChekingButton.Location = new System.Drawing.Point(90, 3);
            this.StopChekingButton.Name = "StopChekingButton";
            this.StopChekingButton.Size = new System.Drawing.Size(75, 41);
            this.StopChekingButton.TabIndex = 5;
            this.StopChekingButton.UseVisualStyleBackColor = false;
            this.StopChekingButton.Click += new System.EventHandler(this.StopChekingButton_Click);
            // 
            // CheckUpdateButton
            // 
            this.CheckUpdateButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CheckUpdateButton.Image = global::proxy.Properties.Resources.Refresh;
            this.CheckUpdateButton.Location = new System.Drawing.Point(8, 3);
            this.CheckUpdateButton.Name = "CheckUpdateButton";
            this.CheckUpdateButton.Size = new System.Drawing.Size(75, 41);
            this.CheckUpdateButton.TabIndex = 4;
            this.CheckUpdateButton.UseVisualStyleBackColor = false;
            this.CheckUpdateButton.Click += new System.EventHandler(this.CheckUpdateButton_Click);
            // 
            // UpdateInfoLabel
            // 
            this.UpdateInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdateInfoLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UpdateInfoLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.UpdateInfoLabel.Location = new System.Drawing.Point(171, 7);
            this.UpdateInfoLabel.Name = "UpdateInfoLabel";
            this.UpdateInfoLabel.Size = new System.Drawing.Size(417, 18);
            this.UpdateInfoLabel.TabIndex = 3;
            this.UpdateInfoLabel.Text = "Проверка не производилась";
            this.UpdateInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ConnectionStatusLabel
            // 
            this.ConnectionStatusLabel.AutoSize = true;
            this.ConnectionStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionStatusLabel.Location = new System.Drawing.Point(48, 7);
            this.ConnectionStatusLabel.Name = "ConnectionStatusLabel";
            this.ConnectionStatusLabel.Size = new System.Drawing.Size(0, 20);
            this.ConnectionStatusLabel.TabIndex = 0;
            // 
            // panelBottom
            // 
            this.panelBottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBottom.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panelBottom.Controls.Add(this.LabelConnectionStatus);
            this.panelBottom.Controls.Add(this.label1);
            this.panelBottom.Controls.Add(this.pictureConnStatus);
            this.panelBottom.Location = new System.Drawing.Point(1, 416);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(630, 29);
            this.panelBottom.TabIndex = 2;
            // 
            // LabelConnectionStatus
            // 
            this.LabelConnectionStatus.AutoSize = true;
            this.LabelConnectionStatus.Location = new System.Drawing.Point(41, 8);
            this.LabelConnectionStatus.Name = "LabelConnectionStatus";
            this.LabelConnectionStatus.Size = new System.Drawing.Size(117, 13);
            this.LabelConnectionStatus.TabIndex = 6;
            this.LabelConnectionStatus.Text = "LabelConnectionStatus";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(54, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 5;
            // 
            // pictureConnStatus
            // 
            this.pictureConnStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureConnStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureConnStatus.Location = new System.Drawing.Point(8, 3);
            this.pictureConnStatus.Name = "pictureConnStatus";
            this.pictureConnStatus.Size = new System.Drawing.Size(27, 26);
            this.pictureConnStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureConnStatus.TabIndex = 4;
            this.pictureConnStatus.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.Controls.Add(this.dataGridViewNotify);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(630, 444);
            this.panel3.TabIndex = 3;
            // 
            // dataGridViewNotify
            // 
            this.dataGridViewNotify.AllowUserToAddRows = false;
            this.dataGridViewNotify.AllowUserToDeleteRows = false;
            this.dataGridViewNotify.AllowUserToResizeColumns = false;
            this.dataGridViewNotify.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dataGridViewNotify.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewNotify.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewNotify.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewNotify.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewNotify.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewNotify.ColumnHeadersVisible = false;
            this.dataGridViewNotify.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChangeDisc,
            this.Date,
            this.Config_Name,
            this.Discription,
            this.DownloadStatus});
            this.dataGridViewNotify.ContextMenuStrip = this.contextMenuHistoryTable;
            this.dataGridViewNotify.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewNotify.EnableHeadersVisualStyles = false;
            this.dataGridViewNotify.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dataGridViewNotify.Location = new System.Drawing.Point(0, 70);
            this.dataGridViewNotify.MultiSelect = false;
            this.dataGridViewNotify.Name = "dataGridViewNotify";
            this.dataGridViewNotify.ReadOnly = true;
            this.dataGridViewNotify.RowHeadersVisible = false;
            this.dataGridViewNotify.RowTemplate.Height = 66;
            this.dataGridViewNotify.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewNotify.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewNotify.Size = new System.Drawing.Size(630, 346);
            this.dataGridViewNotify.TabIndex = 0;
            this.dataGridViewNotify.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewNotify_DataError);
            this.dataGridViewNotify.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewNotify_MouseDown);
            // 
            // contextMenuHistoryTable
            // 
            this.contextMenuHistoryTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SelectionID,
            this.SelectionDay,
            this.CancleSelection,
            this.toolStripSeparator1,
            this.DownloadItem});
            this.contextMenuHistoryTable.Name = "contextMenuHistoryTable";
            this.contextMenuHistoryTable.ShowCheckMargin = true;
            this.contextMenuHistoryTable.Size = new System.Drawing.Size(283, 98);
            this.contextMenuHistoryTable.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuHistoryTable_Opening);
            this.contextMenuHistoryTable.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuHistoryTable_ItemClicked);
            // 
            // SelectionID
            // 
            this.SelectionID.CheckOnClick = global::proxy.Properties.Settings.Default.SelctionHistoryTableID;
            this.SelectionID.Name = "SelectionID";
            this.SelectionID.Size = new System.Drawing.Size(282, 22);
            this.SelectionID.Tag = global::proxy.Properties.Settings.Default.SelectedID;
            this.SelectionID.Text = "Отбор по текущей конфигурации";
            // 
            // SelectionDay
            // 
            this.SelectionDay.Checked = global::proxy.Properties.Settings.Default.SelectionHistoryTableDay;
            this.SelectionDay.Name = "SelectionDay";
            this.SelectionDay.Size = new System.Drawing.Size(282, 22);
            this.SelectionDay.Tag = global::proxy.Properties.Settings.Default.SelectedDay;
            this.SelectionDay.Text = "Текущий день";
            // 
            // CancleSelection
            // 
            this.CancleSelection.Name = "CancleSelection";
            this.CancleSelection.Size = new System.Drawing.Size(282, 22);
            this.CancleSelection.Text = "Отменить отбор";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(279, 6);
            // 
            // DownloadItem
            // 
            this.DownloadItem.Name = "DownloadItem";
            this.DownloadItem.Size = new System.Drawing.Size(282, 22);
            this.DownloadItem.Text = "Загрузить";
            // 
            // imageStatusColumn1
            // 
            this.imageStatusColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imageStatusColumn1.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn1.DefaultCellStyle = dataGridViewCellStyle5;
            this.imageStatusColumn1.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.imageStatusColumn1.HeaderText = "ChangeDisc";
            this.imageStatusColumn1.MinimumWidth = 64;
            this.imageStatusColumn1.Name = "imageStatusColumn1";
            this.imageStatusColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.imageStatusColumn1.Width = 64;
            // 
            // downloadStatusColumn1
            // 
            this.downloadStatusColumn1.DataPropertyName = "DownloadProgress";
            this.downloadStatusColumn1.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn1.HeaderText = "DownloadStatus";
            this.downloadStatusColumn1.Name = "downloadStatusColumn1";
            this.downloadStatusColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imageStatusColumn2
            // 
            this.imageStatusColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imageStatusColumn2.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn2.DefaultCellStyle = dataGridViewCellStyle6;
            this.imageStatusColumn2.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.imageStatusColumn2.HeaderText = "ChangeDisc";
            this.imageStatusColumn2.MinimumWidth = 64;
            this.imageStatusColumn2.Name = "imageStatusColumn2";
            this.imageStatusColumn2.ReadOnly = true;
            this.imageStatusColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.imageStatusColumn2.Width = 64;
            // 
            // downloadStatusColumn2
            // 
            this.downloadStatusColumn2.DataPropertyName = "DownloadProgress";
            this.downloadStatusColumn2.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn2.HeaderText = "DownloadStatus";
            this.downloadStatusColumn2.Name = "downloadStatusColumn2";
            this.downloadStatusColumn2.ReadOnly = true;
            this.downloadStatusColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imageStatusColumn3
            // 
            this.imageStatusColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imageStatusColumn3.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn3.DefaultCellStyle = dataGridViewCellStyle7;
            this.imageStatusColumn3.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.imageStatusColumn3.HeaderText = "ChangeDisc";
            this.imageStatusColumn3.MinimumWidth = 64;
            this.imageStatusColumn3.Name = "imageStatusColumn3";
            this.imageStatusColumn3.ReadOnly = true;
            this.imageStatusColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.imageStatusColumn3.Width = 64;
            // 
            // downloadStatusColumn3
            // 
            this.downloadStatusColumn3.DataPropertyName = "DownloadProgress";
            this.downloadStatusColumn3.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn3.HeaderText = "DownloadStatus";
            this.downloadStatusColumn3.Name = "downloadStatusColumn3";
            this.downloadStatusColumn3.ReadOnly = true;
            this.downloadStatusColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imageStatusColumn4
            // 
            this.imageStatusColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imageStatusColumn4.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn4.DefaultCellStyle = dataGridViewCellStyle8;
            this.imageStatusColumn4.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.imageStatusColumn4.HeaderText = "ChangeDisc";
            this.imageStatusColumn4.MinimumWidth = 64;
            this.imageStatusColumn4.Name = "imageStatusColumn4";
            this.imageStatusColumn4.ReadOnly = true;
            this.imageStatusColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.imageStatusColumn4.Width = 64;
            // 
            // downloadStatusColumn4
            // 
            this.downloadStatusColumn4.DataPropertyName = "DownloadProgress";
            this.downloadStatusColumn4.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn4.HeaderText = "DownloadStatus";
            this.downloadStatusColumn4.Name = "downloadStatusColumn4";
            this.downloadStatusColumn4.ReadOnly = true;
            this.downloadStatusColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imageStatusColumn5
            // 
            this.imageStatusColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imageStatusColumn5.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn5.DefaultCellStyle = dataGridViewCellStyle9;
            this.imageStatusColumn5.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.imageStatusColumn5.HeaderText = "ChangeDisc";
            this.imageStatusColumn5.MinimumWidth = 64;
            this.imageStatusColumn5.Name = "imageStatusColumn5";
            this.imageStatusColumn5.ReadOnly = true;
            this.imageStatusColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.imageStatusColumn5.Width = 64;
            // 
            // downloadStatusColumn5
            // 
            this.downloadStatusColumn5.DataPropertyName = "DownloadProgress";
            this.downloadStatusColumn5.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn5.HeaderText = "DownloadStatus";
            this.downloadStatusColumn5.Name = "downloadStatusColumn5";
            this.downloadStatusColumn5.ReadOnly = true;
            this.downloadStatusColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imageStatusColumn6
            // 
            this.imageStatusColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imageStatusColumn6.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn6.DefaultCellStyle = dataGridViewCellStyle10;
            this.imageStatusColumn6.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.imageStatusColumn6.HeaderText = "ChangeDisc";
            this.imageStatusColumn6.MinimumWidth = 64;
            this.imageStatusColumn6.Name = "imageStatusColumn6";
            this.imageStatusColumn6.ReadOnly = true;
            this.imageStatusColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.imageStatusColumn6.Width = 64;
            // 
            // downloadStatusColumn6
            // 
            this.downloadStatusColumn6.DataPropertyName = "DownloadProgress";
            this.downloadStatusColumn6.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn6.HeaderText = "DownloadStatus";
            this.downloadStatusColumn6.Name = "downloadStatusColumn6";
            this.downloadStatusColumn6.ReadOnly = true;
            this.downloadStatusColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imageStatusColumn7
            // 
            this.imageStatusColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imageStatusColumn7.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn7.DefaultCellStyle = dataGridViewCellStyle11;
            this.imageStatusColumn7.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.imageStatusColumn7.HeaderText = "ChangeDisc";
            this.imageStatusColumn7.MinimumWidth = 64;
            this.imageStatusColumn7.Name = "imageStatusColumn7";
            this.imageStatusColumn7.ReadOnly = true;
            this.imageStatusColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.imageStatusColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.imageStatusColumn7.Width = 64;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Date";
            this.dataGridViewTextBoxColumn1.HeaderText = "Date";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn2.HeaderText = "Config_Name";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Discription";
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn3.HeaderText = "Discription";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // downloadStatusColumn7
            // 
            this.downloadStatusColumn7.DataPropertyName = "DownloadProgress";
            this.downloadStatusColumn7.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.downloadStatusColumn7.HeaderText = "DownloadStatus";
            this.downloadStatusColumn7.Name = "downloadStatusColumn7";
            this.downloadStatusColumn7.ReadOnly = true;
            this.downloadStatusColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.FillWeight = 152.2843F;
            this.dataGridViewImageColumn1.HeaderText = "Column1";
            this.dataGridViewImageColumn1.MinimumWidth = 64;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 208;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.FillWeight = 73.85786F;
            this.dataGridViewImageColumn2.HeaderText = "Column3";
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.ReadOnly = true;
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn2.Width = 208;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuNotify;
            this.notifyIcon1.Text = "Проверка обновлений 1С";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // contextMenuNotify
            // 
            this.contextMenuNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem});
            this.contextMenuNotify.Name = "contextMenuNotify";
            this.contextMenuNotify.Size = new System.Drawing.Size(109, 26);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // ChangeDisc
            // 
            this.ChangeDisc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ChangeDisc.DataPropertyName = "ChangeDisc";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ChangeDisc.DefaultCellStyle = dataGridViewCellStyle2;
            this.ChangeDisc.DefaultStatus = proxy.DataBase.ProductUpdateEvent.Plan;
            this.ChangeDisc.HeaderText = "ChangeDisc";
            this.ChangeDisc.MinimumWidth = 64;
            this.ChangeDisc.Name = "ChangeDisc";
            this.ChangeDisc.ReadOnly = true;
            this.ChangeDisc.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ChangeDisc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ChangeDisc.Width = 64;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "Date";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // Config_Name
            // 
            this.Config_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Config_Name.DataPropertyName = "Name";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Config_Name.DefaultCellStyle = dataGridViewCellStyle3;
            this.Config_Name.HeaderText = "Config_Name";
            this.Config_Name.MinimumWidth = 100;
            this.Config_Name.Name = "Config_Name";
            this.Config_Name.ReadOnly = true;
            // 
            // Discription
            // 
            this.Discription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Discription.DataPropertyName = "Discription";
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Discription.DefaultCellStyle = dataGridViewCellStyle4;
            this.Discription.HeaderText = "Discription";
            this.Discription.MinimumWidth = 40;
            this.Discription.Name = "Discription";
            this.Discription.ReadOnly = true;
            this.Discription.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // DownloadStatus
            // 
            this.DownloadStatus.DataPropertyName = "DownloadProgress";
            this.DownloadStatus.DefaultStatus = proxy.DataBase.DownloadProgress.Waiting;
            this.DownloadStatus.HeaderText = "DownloadStatus";
            this.DownloadStatus.Name = "DownloadStatus";
            this.DownloadStatus.ReadOnly = true;
            this.DownloadStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 446);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelUp);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Text = "1С Updates";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing_1);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelUp.ResumeLayout(false);
            this.panelUp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStatus)).EndInit();
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureConnStatus)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNotify)).EndInit();
            this.contextMenuHistoryTable.ResumeLayout(false);
            this.contextMenuNotify.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem файлToolStripMenuItem;
        private ToolStripMenuItem ToolStripMenuItem;
        private ToolStripMenuItem настройкиToolStripMenuItem;
        private Panel panelUp;
        private ToolStripMenuItem SettingsToolStripMenuItem;
        private ToolStripMenuItem справкаToolStripMenuItem;
        private ToolStripMenuItem оПрограммеToolStripMenuItem;
        private Label ConnectionStatusLabel;
        private Panel panelBottom;
        private Panel panel3;
        private PictureBox pictureConnStatus;
        private Label UpdateInfoLabel;
        private DataGridViewImageColumn dataGridViewImageColumn1;
        private DataGridViewImageColumn dataGridViewImageColumn2;
        private Button CheckUpdateButton;
        private Button StopChekingButton;
        private Label label1;
        private Label LabelConnectionStatus;
        private ContextMenuStrip contextMenuHistoryTable;
        private PictureBox pictureStatus;
        private DataGridView dataGridViewNotify;
        private ImageStatusColumn imageStatusColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private ToolStripMenuItem SelectionID;
        private ToolStripMenuItem SelectionDay;
        private ToolStripMenuItem CancleSelection;
        private Label UpdateInfoLabelAdd;
        private DownloadStatusColumn downloadStatusColumn1;
        private ImageStatusColumn imageStatusColumn2;
        private DownloadStatusColumn downloadStatusColumn2;
        private ImageStatusColumn imageStatusColumn3;
        private DownloadStatusColumn downloadStatusColumn3;
        private ImageStatusColumn imageStatusColumn4;
        private DownloadStatusColumn downloadStatusColumn4;
        private ImageStatusColumn imageStatusColumn5;
        private DownloadStatusColumn downloadStatusColumn5;
        private ImageStatusColumn imageStatusColumn6;
        private DownloadStatusColumn downloadStatusColumn6;
        private ImageStatusColumn imageStatusColumn7;
        private DownloadStatusColumn downloadStatusColumn7;
        private NotifyIcon notifyIcon1;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem DownloadItem;
        private ToolStripMenuItem выборКонфигурацийToolStripMenuItem;
        private ContextMenuStrip contextMenuNotify;
        private ToolStripMenuItem выходToolStripMenuItem;
        private ImageStatusColumn ChangeDisc;
        private DataGridViewTextBoxColumn Date;
        private DataGridViewTextBoxColumn Config_Name;
        private DataGridViewTextBoxColumn Discription;
        private DownloadStatusColumn DownloadStatus;
    }
}