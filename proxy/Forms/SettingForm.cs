﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using proxy.Common;
using proxy.Configs;
using proxy.Logger;
using proxy.Properties;

namespace proxy.Forms
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            textBoxPass.Text = Settings.Default.Password;

            if (Settings.Default.UseDefaultUpdFolder)
            {
                textBoUpdFolder.Enabled = false;
                buttonFolderDialog.Enabled = false;
            }

            //Загрузка раздела периодов проверки обновлений
            for (int i = 0; i < Settings.Default.DaysForCheck.Count; i++)
            {
                checkedListBoxDaysOfWeek.SetItemChecked(i, bool.Parse(Settings.Default.DaysForCheck[i]));
            }


            //Интеравал проверки
            textBoxCheckInterval.Text = Settings.Default.CheckUpdatesInterval.ToString();


        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //  Properties.Settings.Default.DaysForCheck.Clear();

            if (textBoxPass.Text != string.Empty && Settings.Default.Password != textBoxPass.Text)
            {
                Settings.Default.Password = Helpers.Encrypt(textBoxPass.Text, true);
            }
            else if (textBoxPass.Text == string.Empty)
            {
                Settings.Default.Password = string.Empty;
            }

            for (int i = 0; i < Settings.Default.DaysForCheck.Count; i++)
            {
                Settings.Default.DaysForCheck[i] = "false";
            }

            foreach (int item in checkedListBoxDaysOfWeek.CheckedIndices)
            {
                Settings.Default.DaysForCheck[item] = "true";
            }


            //Автозагрузка в реестре
            if (Helpers.SetAutorunRegValue(Settings.Default.Autostart) == false)
            {
                LogWriter.Instance.WriteToLog("Реестр не доступен", LogLevel.Error);
                Settings.Default.Autostart = false;
            }


            int checkInterval = 0;
            if (int.TryParse(textBoxCheckInterval.Text, out checkInterval))
            {
                Settings.Default.CheckUpdatesInterval = checkInterval;
            }

            if (dateTimeFrom.Value >= dateTimeTimeTo.Value)
            {
                MessageBox.Show("Некорректный период проверки", "Внимание!");
                e.Cancel = true;
                return;
            }


            int r = 0;
            if (!int.TryParse(textBoxCheckInterval.Text, out r) || r < 20)
            {

                MessageBox.Show("Периодичность проверки в диапазоне 20 - 999");
                e.Cancel = true;
                return;
            }

            Settings.Default.DownloadUpdates = true;

            Settings.Default.Save();

        }

        private void maskedTextTime_Validating(object sender, CancelEventArgs e)
        {
            MaskedTextBox _mt = sender as MaskedTextBox;

            int _i;
            int.TryParse(_mt.Text.Replace(":", ""), out _i);

            if (_i > 2359 || _mt.MaskedTextProvider.MaskFull != true)
            {
                e.Cancel = true;
                MessageBox.Show("Некорректный период проверки");

            }

        }

        private void buttonFolderDialog_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.SelectedPath = textBoUpdFolder.Text;
                if (dialog.ShowDialog() == DialogResult.OK)
                    textBoUpdFolder.Text = dialog.SelectedPath;
            }

        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox s = (CheckBox)sender;

            if (s.Checked)
            {
                textBoUpdFolder.Text = SettingsManager.DEFAULT_UPD_FOLDER;
                textBoUpdFolder.Enabled = false;
                buttonFolderDialog.Enabled = false;
                s.Checked = true;
            }
            else
            {
                textBoUpdFolder.Enabled = true;
                buttonFolderDialog.Enabled = true;
            }
        }





        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        //        return cp;
        //    }
        //} 


    }
}
