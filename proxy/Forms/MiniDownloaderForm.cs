﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows.Forms;
using proxy.Common;
using proxy.Download;

namespace proxy.Forms
{
    public partial class MiniDownloaderForm : Form
    {

        public DataRowView HistoryRow;
        BindingList<DownloadFileInfo> fileList = new BindingList<DownloadFileInfo>();

        public MiniDownloaderForm()
        {
            InitializeComponent();
        }

        private void MiniDownloaderForm_Load(object sender, EventArgs e)
        {
            Text += String.Format("{0} {1} {2}", "(", HistoryRow["Name"], ")"); 
            miniDownloaderGridView.AutoGenerateColumns = false;            
            GenerateFilesList();
            miniDownloaderGridView.DataSource = fileList;

            for (int i = 0; i < miniDownloaderGridView.Rows.Count; i++)
            {
                miniDownloaderGridView.Rows[i].Cells["NeedDownload"].Value = false;
            }
            miniDownloaderGridView.EndEdit();
        }


        private void GenerateFilesList()
        {
            
            if (HistoryRow != null)
            {
                int _index = HistoryRow.Row.Table.Rows.IndexOf(HistoryRow.Row);
                
                //Если новая версия
                if ((DataBase.ProductUpdateEvent)HistoryRow.Row["ChangeDisc"] == DataBase.ProductUpdateEvent.Release 
                    || (DataBase.ProductUpdateEvent)HistoryRow.Row["ChangeDisc"] == DataBase.ProductUpdateEvent.ReviewVersion)
                {
                    foreach (var item in DataBase.Instance.Content.Select("ID = '" + HistoryRow["ID"] + "' AND Version = '" + HistoryRow["EventVersion"] + "'"))
                    {
                        fileList.Add(new DownloadFileInfo(item["href"].ToString(), item["ID"] + "\\" + item["Version"] + "\\", item["filename"].ToString(), 0, item["discription"].ToString()));
                    }
                }

                //Если обновление внешних файлов продукта
                if ((DataBase.ProductUpdateEvent)HistoryRow.Row["ChangeDisc"] == DataBase.ProductUpdateEvent.ProductContent)
                {
                    foreach (var item in DataBase.Instance.Extrafiles.Select("ID = '" + HistoryRow["ID"] + "' AND discription = '" + HistoryRow["EventVersion"] + "'"))
                    {
                        fileList.Add(new DownloadFileInfo(item["href"].ToString(), item["ID"] + "\\" + "Extra" + "\\", item["filename"].ToString(), 0, item["discription"].ToString()));
                    }
                }


            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox s = (CheckBox)sender;

            if (s.Checked)
            {
                for (int i = 0; i < miniDownloaderGridView.Rows.Count; i++)
                {
                    miniDownloaderGridView.Rows[i].Cells["NeedDownload"].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < miniDownloaderGridView.Rows.Count; i++)
                {
                    miniDownloaderGridView.Rows[i].Cells["NeedDownload"].Value = false;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            foreach (DataGridViewRow item in miniDownloaderGridView.Rows)
            {
                if ((bool)item.Cells["NeedDownload"].Value)
                {
                   DownloadFileInfo dfi =  item.DataBoundItem as DownloadFileInfo;
                   DownloadManager.Instance.Download(dfi);
                }           
            }
        }

        private void buttonOpenFilesFolder_Click(object sender, EventArgs e)
        {
            if (fileList.Count > 0)
            {
                if (!Helpers.OpenFolder(Path.Combine(Helpers.GetUpdatesFolder(), fileList[0].FileSaveDirectory)))
                    MessageBox.Show("Путь не обнаружен", "Внимание!");
            }
        }


    }
}

