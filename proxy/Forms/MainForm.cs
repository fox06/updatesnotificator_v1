﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using proxy.Common;
using proxy.Connection;
using proxy.Logger;
using proxy.Properties;
//using SpannedDataGridView;

namespace proxy.Forms
{
    public partial class MainForm : Form
    {
        DataView dataView;
        BindingSource bs;
        DataTable tempDataTable;

        private FormWindowState _OldFormState;

        delegate void Add_history(DataRow dr);
        delegate void Connection_Status(ConnectionStatus cs, string i);
        delegate void Upadates_Status(UpdatesStatus us, string info);

      
        public MainForm()
        {            
            InitializeComponent();
            Text = Application.ProductName + " v" + Application.ProductVersion + Helpers.GetAssembly();
            notifyIcon1.Icon = Resources.icon1;
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
           OnChangeConnectionStatus(this, new OnChangeConnectionStatusHandler(ConnectionStatus.NoConnection));

            dataGridViewNotify.AutoGenerateColumns = false;
            dataView = DataBase.Instance.History.DefaultView;
            dataView.Sort = "Date DESC";            
            bs = new BindingSource();
            bs.DataSource = dataView;
            dataGridViewNotify.DataSource = bs;
            
            
            SetSelection();
        }
          

        public void OnChangeConnectionStatus(object sender, OnChangeConnectionStatusHandler e)
        {
            if (IsHandleCreated)
                BeginInvoke(new Connection_Status(ChangeConnectionStatusLabel), e.ConnectionStatus, e.Info);
        }

        public void OnUpdatesEvent(object sender, UpdatesEventHandler e)
        {
            if (IsHandleCreated)
                BeginInvoke(new Upadates_Status(ChangeUpdatesStatusLabel), e.UpdatesStatus, e.Info);
        }

        private void ChangeUpdatesStatusLabel(UpdatesStatus us, string info)
        {
           // Logger.LogWriter.Instance.WriteToLog(us.ToString() + "::::::" + info);

            if (us == UpdatesStatus.InProgress)
            {
                dataGridViewNotify.SuspendLayout();
                tempDataTable = dataView.ToTable();
                bs.DataSource = null;
                bs.DataSource = tempDataTable;
                dataGridViewNotify.Enabled = false;
                
                UpdateInfoLabel.Text = info;
                pictureStatus.Image = Resources.loader;                
                menuStrip1.Enabled = false;
                CheckUpdateButton.Enabled = false;
                StopChekingButton.Enabled = !(CheckUpdateButton.Enabled);
                Application.UseWaitCursor = true;                
            }
            else if (us == UpdatesStatus.AddiditionInfo)
            {
                UpdateInfoLabelAdd.Text = info;
            }
            else if (us == UpdatesStatus.IsCompleted)
            {

                if (WindowState == FormWindowState.Minimized)
                {
                    notifyIcon1.ShowBalloonTip(10, "", "Получены обновления", ToolTipIcon.Info);
                }

                UpdateInfoLabel.Text = info;
                pictureStatus.Image = Resources.onebit_02;
                Application.UseWaitCursor = false;
                menuStrip1.Enabled = true;
                CheckUpdateButton.Enabled = true;
                StopChekingButton.Enabled = !(CheckUpdateButton.Enabled);


                dataGridViewNotify.ResumeLayout();
                bs.DataSource = dataView;
                dataGridViewNotify.Refresh();
                dataGridViewNotify.Enabled = true;
                tempDataTable.Dispose();

                dataGridViewNotify.Enabled = true;

                DataBase.SaveDataBase();
          
            }
            else if (us == UpdatesStatus.Error)
            {

                UpdateInfoLabel.Text = info;
                pictureStatus.Image = Resources.onebit_06;
                Application.UseWaitCursor = false;
                menuStrip1.Enabled = true;
                CheckUpdateButton.Enabled = true;
                StopChekingButton.Enabled = !(CheckUpdateButton.Enabled);

                dataGridViewNotify.ResumeLayout();
                dataGridViewNotify.Enabled = true;
            }
            else if (us == UpdatesStatus.DatabaseLoading)
            {
                UpdateInfoLabel.Text = "Загрузка базы данных";
                pictureStatus.Image = Resources.loader;
                UseWaitCursor = false;
                Enabled = false;
                dataGridViewNotify.ResumeLayout();
                dataGridViewNotify.Enabled = false;
            }
            else if (us == UpdatesStatus.DatabaseSaving)
            {
                UpdateInfoLabel.Text = "Сохранение базы данных";
                pictureStatus.Image = Resources.loader;
                UseWaitCursor = false;
                Enabled = false;
                dataGridViewNotify.ResumeLayout();
                dataGridViewNotify.Enabled = false;
            }

        }

        private void ChangeConnectionStatusLabel(ConnectionStatus cs, string info)
        {
            if (cs == ConnectionStatus.Connected)
            {
                pictureConnStatus.Image = Resources.picIsConnected;
                LabelConnectionStatus.Text = "Соединение установленно " + info;
            }
            else if (cs == ConnectionStatus.Reconnecting)
            {
                pictureConnStatus.Image = Resources.picNoConnection;
                LabelConnectionStatus.Text = "Попытка подключения к серверу " + info;
            }
            else if (cs == ConnectionStatus.ConnectionError )
            {
                pictureConnStatus.Image = Resources.picNoConnection;
                LabelConnectionStatus.Text = "Ошибка соединения с сервером " + info;
            }

            else if (cs ==  ConnectionStatus.NoConnection)
            {
                pictureConnStatus.Image = Resources.picNoConnection;
                LabelConnectionStatus.Text = "Соединение отсутствует " + info;
            }

            else if (cs == ConnectionStatus.WrongLoginPassword)
            {
                pictureConnStatus.Image = Resources.picNoConnection;
                LabelConnectionStatus.Text = "Неверные логин или пароль " + info;
            }

        }

        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SettingsForm settingForm = new SettingsForm();
                settingForm.ShowDialog(this);
                SetSelection();
            }
            catch (Exception ex)
            {
               
            }

        }


        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProgramManager.Instance.OnExitApp();
        }

        private void CheckUpdateButton_Click(object sender, EventArgs e)
        {           
            menuStrip1.Enabled = false;
            CheckUpdateButton.Enabled = false;

            ProgramManager.Instance.StartChecking();
        }


        
        private void MainForm_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            if (Settings.Default.ToTray && e.CloseReason != CloseReason.ApplicationExitCall)
            {
                e.Cancel = true;
                Hide();
            }
            else
            {
                ProgramManager.Instance.OnExitApp();
            }
        }

        private void dataGridViewNotify_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int currentMouseOverRow = dataGridViewNotify.HitTest(e.X, e.Y).RowIndex;

                if (currentMouseOverRow >= 0)
                {
                    dataGridViewNotify.Rows[currentMouseOverRow].Selected = true;
                }
            }
        }
            


        private void contextMenuHistoryTable_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == contextMenuHistoryTable.Items["SelectionID"] && dataGridViewNotify.SelectedRows.Count > 0)
            {
                DataRowView HistoryRow = dataGridViewNotify.SelectedRows[0].DataBoundItem as DataRowView;
                if (HistoryRow != null)
                {
                    e.ClickedItem.Tag = HistoryRow.Row["ID"].ToString();
                    Settings.Default.SelectedID = HistoryRow.Row["ID"].ToString();
                    SetSelection();
                }
            }
            if (e.ClickedItem == contextMenuHistoryTable.Items["SelectionDay"])
            {
                DateTime SelectedDay = DateTime.Today;
                DateTime EndOfDay = new DateTime(SelectedDay.Year, SelectedDay.Month, SelectedDay.Day, 23, 59, 59);
                Settings.Default.SelectedDay = SelectedDay;
                SetSelection();
            }
            if (e.ClickedItem == contextMenuHistoryTable.Items["CancleSelection"])
            {
                DropSelection();
                return;
            }

 
            if (e.ClickedItem == contextMenuHistoryTable.Items["DownloadItem"])
            {
                MiniDownloaderForm mdf = new MiniDownloaderForm();
                mdf.HistoryRow = dataGridViewNotify.SelectedRows[0].DataBoundItem as DataRowView;
                mdf.ShowDialog(this);
            }

            
        }
        
      

        private void SetSelection()
        {
            StringBuilder sb = new StringBuilder();
            string _and = "AND";
            string _or = "OR";
            bool _needand = true;

          //  dataView.RowFilter = false.ToString();
            sb.AppendFormat("Parent(id_prod_history).Check = true AND Parent(id_prod_history).Available = true");
            //sb.AppendFormat("Parent(id_prod_history).Available = true");

            if (!Settings.Default.NotifyContentUpd)
            {
                sb.AppendFormat(" {0} {1} <> {2} ", _needand ? _and : String.Empty, "ChangeDisc" ,(int)DataBase.ProductUpdateEvent.ProductContent);
                _needand = true;
            }

            if (!Settings.Default.NotifyContentUpd)
            {
                sb.AppendFormat(" {0} {1} <> {2} ", _needand ? _and : String.Empty, "ChangeDisc", (int)DataBase.ProductUpdateEvent.VersionContent);
                _needand = true;
            }

            if (Settings.Default.SelectedID != String.Empty)
            {
                sb.AppendFormat(" {0} {1} = '{2}' ", _needand ? _and : String.Empty, "ID", Settings.Default.SelectedID);
                _needand = true;
            }

            if (Settings.Default.SelectedDay > DateTime.MinValue)
            {
                DateTime SelectedDay = DateTime.Today;
                DateTime EndOfDay = new DateTime(SelectedDay.Year, SelectedDay.Month, SelectedDay.Day, 23, 59, 59);
                sb.AppendFormat(" {0}  ({1} >= '{2}' AND {1} <= '{3}') ", _needand ? _and : String.Empty, "Date", SelectedDay, EndOfDay);
            }

            sb.AppendFormat(" OR ChangeDisc = " + (int)DataBase.ProductUpdateEvent.NewProductAvailable);
            sb.AppendFormat(" OR ChangeDisc = " + (int)DataBase.ProductUpdateEvent.PruductIsUnavailable);

            dataView.RowFilter = sb.ToString();
            dataGridViewNotify.Refresh();
      
        }

        private void DropSelection()
        {
            Settings.Default.SelectedDay = DateTime.MinValue;
            Settings.Default.SelectedID = String.Empty;
            SetSelection();
        }

        private void StopChekingButton_Click(object sender, EventArgs e)
        {
            ProgramManager.Instance.Stop();
            UseWaitCursor = false;            
        }


        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            //проверяем, какой кнопкой было произведено нажатие
            if (e.Button == MouseButtons.Left)//если левой кнопкой мыши
            {
                //проверяем текущее состояние окна
                if (WindowState == FormWindowState.Normal || WindowState == FormWindowState.Maximized)//если оно развернуто
                {
                    //сохраняем текущее состояние
                    dataGridViewNotify.SuspendLayout();
                    _OldFormState = WindowState;
                    //сворачиваем окно
                    WindowState = FormWindowState.Minimized;
                    //скрываться в трей оно будет по событию Resize (изменение размера), которое сгенерировалось после минимизации строчкой выше
                }
                else//в противном случае
                {
                    //и показываем на нанели задач
                    Show();
                    //разворачиваем (возвращаем старое состояние "до сворачивания")
                    WindowState = _OldFormState;
                    dataGridViewNotify.ResumeLayout();
                }
            } 
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (Settings.Default.ToTray && FormWindowState.Minimized == WindowState)
                Hide();
            
        }

        private void contextMenuHistoryTable_Opening(object sender, CancelEventArgs e)
        {
            if (dataGridViewNotify.SelectedRows.Count < 1 || dataGridViewNotify.Enabled == false)
            {
                e.Cancel = true;
                return;
            }

             DataRowView HistoryRow = dataGridViewNotify.SelectedRows[0].DataBoundItem as DataRowView;

             if (HistoryRow != null && (DataBase.ProductUpdateEvent)HistoryRow.Row["ChangeDisc"] == DataBase.ProductUpdateEvent.Plan 
                 || (DataBase.DownloadProgress)HistoryRow.Row["DownloadProgress"] == DataBase.DownloadProgress.Downloading)
                contextMenuHistoryTable.Items["DownloadItem"].Enabled = false;
             else
                contextMenuHistoryTable.Items["DownloadItem"].Enabled = true;
             
        }

        private void dataGridViewNotify_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            LogWriter.Instance.WriteToLog(e.ToString());
            //throw new Exception(e.Exception.Message.ToString());
        }

        private void выборКонфигурацийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductsSelectionForm psf = new ProductsSelectionForm();
            psf.ShowDialog();
            SetSelection();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProgramManager.Instance.OnExitApp();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox form = new AboutBox();
            form.ShowDialog(this);

        }

    }    
}
