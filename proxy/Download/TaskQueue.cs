﻿using System;
using System.Collections.Generic;
using System.Threading;
using proxy.Configs;

namespace proxy.Download
{
    /// <summary>
    /// Description of TaskQueue
    /// </summary>
    public sealed class DownloadQueue : IDisposable
    {
        private static DownloadQueue instance = new DownloadQueue();

        object locker = new object();

        readonly Thread[] _workers;
        private Queue<DownloadFileInfo> _taskQ = new Queue<DownloadFileInfo>();

        public Queue<DownloadFileInfo> TaskQ
        {
            get { return _taskQ; }
            private set { _taskQ = value; }
        }


        int _workerCount;

        public int WorkerCount
        {
            get { return _workerCount; }
            set { _workerCount = value; }
        }



        public static DownloadQueue Instance
        {
            get
            {
                return instance;
            }
        }

        private DownloadQueue()
        {
            _workers = new Thread[SettingsManager.DOWNLOAD_SLOTS_NUM];
        }

        public void Start()
        {

            // Создать и запустить отдельный поток на каждого потребителя
            for (int i = 0; i < _workerCount; i++)
                (_workers[i] = new Thread(Consume)).Start(i);
        }

        public void EnqueueTask(DownloadFileInfo task)
        {
            lock (locker)
            {
                _taskQ.Enqueue(task);
                Monitor.PulseAll(locker);
            }
        }


        void Consume(object i)
        {
            while (true)
            {
                DownloadFileInfo task;

                lock (locker)
                {
                    while (_taskQ.Count == 0)
                        Monitor.Wait(locker);


                    task = _taskQ.Dequeue();
                }



                if (task == null)
                    return;  // Сигнал на выход

                task.Start((int)i);

            }
        }

        public void Dispose()
        {
            // Добавить по null-задаче на каждого завершаемого потребителя
            foreach (Thread worker in _workers)
                EnqueueTask(null);

            foreach (Thread worker in _workers)
                worker.Join();
        }
    }
}
