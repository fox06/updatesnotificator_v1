﻿using System;
using System.IO;
using System.Net;
using System.Text;
using proxy.Common;
using proxy.Configs;
using proxy.Connection;
using proxy.Logger;

namespace proxy.Download
{
    /// <summary>
    /// Description of SingletonClass1
    /// </summary>
    public sealed class DownloadManager : MessagerComponent, IDisposable
    {
        // The delegate which we will call from the thread to update the form
        // public delegate void UpdateProgessCallback(Int64 BytesRead, Int64 TotalBytes);

      //  public event ProgressChangedHandler ProgressChanged;
      //  public List<DownloadFileInfo> Downloadlist = new List<DownloadFileInfo>();

        public string DownloadDirectory;
        const string Postfix = ".download";

        private static DownloadManager instance = new DownloadManager();

        public static DownloadManager Instance
        {
            get
            {
                return instance;
            }
        }

        private DownloadManager()
        {
            DownloadQueue.Instance.WorkerCount = SettingsManager.DOWNLOAD_SLOTS_NUM;
            DownloadQueue.Instance.Start();
        }


        public void Download(DownloadFileInfo file)
        {
            DownloadQueue.Instance.EnqueueTask(file);
        }

        public bool DownloadPage(Uri html, ref string result)
        {
            if (ConnectionManager.Instance.Status != ConnectionStatus.Connected)
            {
                if (ConnectionManager.Instance.SetConnection(1, 1000) == false)
                {
                    return false;
                }
            }

            try
            {
                using (WebClient wcDownload = new WebClient())
                {
                    try
                    {
                        wcDownload.Headers.Add("Cookie", ConnectionManager.Instance.CookieContainer.GetCookieHeader(ConnectionManager.Instance.CookieHostname));
                    }
                    catch (Exception ex)
                    {
                        LogWriter.Instance.WriteToLog(ex.Message + "::::::" + ex.Source + "::::::" + ex.StackTrace);
                        return false;
                    }

                    WebRequest _WebRequest1 = (HttpWebRequest)WebRequest.Create(html);
                    _WebRequest1.Headers.Add("Cookie", ConnectionManager.Instance.CookieContainer.GetCookieHeader(ConnectionManager.Instance.CookieHostname));
                    WebResponse _WebResponse1 = (HttpWebResponse)_WebRequest1.GetResponse();

                    using (Stream strResp = wcDownload.OpenRead(html))
                    {
                        using (StreamReader strLocal = new StreamReader(strResp, Encoding.GetEncoding("windows-1251")))
                        {
                            result = strLocal.ReadToEnd();
                            strResp.Close();
                            strLocal.Close();
                        }
                    }
                }

            }
            catch (WebException e)
            {
                LogWriter.Instance.WriteToLog(e.Source + " " + e.StackTrace);
                return false;
            }

            return true;
        }

        public void DownloadFile(DownloadFileInfo fileInfo)
        {

            using (WebClient wcDownloadD = new WebClient())
            {
                if (ConnectionManager.Instance.CookieContainer == null)
                {
                    ConnectionManager.Instance.SetConnection();

                    if (ConnectionManager.Instance.Status == ConnectionStatus.NoConnection)
                    {
                        fileInfo.Status = FileDownloadStatusEnum.Error;
                    }
                }
                
                wcDownloadD.Proxy = null;
                wcDownloadD.Headers.Add("Cookie", SettingsManager.JSONCODE);
                wcDownloadD.Headers.Add("Referer", fileInfo.FileHttpPath);          

                WebRequest _WebRequest1 = (HttpWebRequest)WebRequest.Create(fileInfo.FileHttpPath);
                _WebRequest1.Headers.Add("Cookie", SettingsManager.JSONCODE);

                Int64 fileSize = 0;

                try
                {
                    WebResponse _WebResponse1 = (HttpWebResponse)_WebRequest1.GetResponse();
                    fileSize = _WebResponse1.ContentLength;
                }
                catch (Exception exception)
                    {
                        LogWriter.Instance.WriteToLog("Download file error : " + fileInfo.FileHttpPath + exception.Message);
                        return;
                    }

              
              

                using (Stream strRespD = wcDownloadD.OpenRead(fileInfo.FileHttpPath))
                {                                       
                    string _dir = Path.Combine(Path.GetFullPath(Helpers.GetUpdatesFolder()), fileInfo.FileSaveDirectory);
                    string fileNameFullPath = Path.Combine(_dir, fileInfo.FileName + Postfix);

                    if (!Directory.Exists(_dir))
                        Directory.CreateDirectory(_dir);
                    
                    File.Delete(fileNameFullPath);


                    using (Stream strLocal = new FileStream(fileNameFullPath, FileMode.Create, FileAccess.Write, FileShare.Delete))
                    {
                        // Хранит количество полученных байт
                        int bytesSize = 0;
                        
                        // Буфер 
                        byte[] downBuffer = new byte[2048];

                        fileInfo.Status = FileDownloadStatusEnum.Downloading;
                        fileInfo.TotalBytes = fileSize;

                        // Проход и освобождение буфера
                        while ((bytesSize = strRespD.Read(downBuffer, 0, downBuffer.Length)) > 0)
                        {
                            // Запись данных буфера на диск
                            strLocal.Write(downBuffer, 0, bytesSize);

                            try
                            {
                                fileInfo.BytesRead = strLocal.Length;
                            }
                            catch (Exception ex)
                            {
                                LogWriter.Instance.WriteToLog(ex.Message);
                            }
                        }

                        fileInfo.Status = FileDownloadStatusEnum.Completed;

                        strRespD.Close();
                        strLocal.Close();

                        string newName = fileNameFullPath.Replace(Postfix, "");

                        if (File.Exists(newName))
                            File.Delete(newName);

                        File.Move(fileNameFullPath, newName);
                    }
                }
            }
        }
        

        public void Dispose()
        {
            DownloadQueue.Instance.Dispose();
        }
        
        //private void OnProgressChanged(Int64 BytesRead, Int64 TotalBytes, DownloadFileInfo fileInfo)
        //{
        //    ProgressChangedHandler handler = this.ProgressChanged;

        //    if (handler != null)
        //    {
        //        handler(this, new ProgressChangedEventArgs { BytesRead = BytesRead, TotalBytes = TotalBytes, downloadFileInfo = fileInfo });
        //    }
        //}

    }
}
