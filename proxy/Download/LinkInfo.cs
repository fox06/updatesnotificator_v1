﻿
namespace proxy.Download
{
    public class LinkInfo
    {
        public string HttpPath { get; set; }

        public string Discription { get; set; }

        public LinkInfo(string http, string discription)
        {
            HttpPath = http;
            Discription = discription;
        }
    }

}
