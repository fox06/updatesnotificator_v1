﻿using System;

namespace proxy.Download
{
    public class ProgressChangedEventArgs : EventArgs
    {
        public long BytesRead { get; set; }
        public long TotalBytes { get; set; }

        public DownloadFileInfo DownloadFileInfo;
    }
}
