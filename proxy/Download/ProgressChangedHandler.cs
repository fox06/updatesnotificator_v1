namespace proxy.Download
{
    public delegate void ProgressChangedHandler(object sender, ProgressChangedEventArgs e);
}