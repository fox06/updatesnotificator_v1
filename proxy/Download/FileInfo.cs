﻿using System;

namespace proxy.Download
{
    public class LinkFileInfo
    {
        public string FileHttpPath { get; set; }

        public string FileName { get; set; }

        public Int64 FileLength { get; set; }

        public string FileDiscription { get; set; }

        public LinkFileInfo(string filePath, string name, long length, string discription)
        {
            FileHttpPath = filePath;
            FileName = name;
            FileLength = length;
            FileDiscription = discription;
        }

    }

}
