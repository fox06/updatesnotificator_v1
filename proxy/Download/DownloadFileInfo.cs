﻿using System;
using System.ComponentModel;

namespace proxy.Download
{
    public class DownloadFileInfo : LinkFileInfo, INotifyPropertyChanged
    {
        private int _progress;
        public int Progress
        {
            get { return _progress; }
            set
            {
                CheckPropertyChanged("Progress", ref _progress, ref value);
            }
        }

        private FileDownloadStatusEnum _status;
        public FileDownloadStatusEnum Status
        {
            get { return _status; }
            set
            {
                CheckPropertyChanged("Status", ref _status, ref value);
            }
        }

        private Int64 _bytesRead;
        public Int64 BytesRead
        {
            get { return _bytesRead; }
            set
            {       
                CheckPropertyChanged("BytesRead", ref _bytesRead, ref value);
                Progress = TotalBytes != 0 ? Convert.ToInt32((BytesRead * 100) / TotalBytes) : 0;
            }
        }

        private Int64 _totalBytes;
        public Int64 TotalBytes
        {
            get { return _totalBytes; }
            set { CheckPropertyChanged("TotalBytes", ref _totalBytes, ref value); }
        }

        private string _fileSaveDirectory;
        public string FileSaveDirectory
        {
            get { return _fileSaveDirectory; }
            set { _fileSaveDirectory = value; }
        }

        public DownloadFileInfo(string fileHref, string directory, string fileName, Int64 lenght, string discription)
            : base(fileHref, fileName, lenght, discription)
        {
            FileSaveDirectory = directory;
        }


        public void Start(int slot)
        {
            DownloadManager.Instance.DownloadFile(this);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;


        protected bool CheckPropertyChanged<T>(string propertyName, ref T oldValue, ref T newValue)
        {
            if (oldValue == null && newValue == null)
            {
                return false;
            }

            if ((oldValue == null && newValue != null) || !oldValue.Equals(newValue))
            {
                oldValue = newValue;

                FirePropertyChanged(propertyName);

                return true;
            }

            return false;
        }

        protected void FirePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
