namespace proxy.Download
{
    public enum FileDownloadStatusEnum
    {
        Downloading,
        Completed,
        Error,
        Waiting
    }
}