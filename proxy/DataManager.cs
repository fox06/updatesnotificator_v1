﻿using System;
using System.Collections.Generic;
using System.Data;
using proxy.Common;
using proxy.Configs;
using proxy.Download;
using proxy.Logger;

namespace proxy
{
    class DataManager
    {
        public event EventHandler<CurrentProcEventHandler> CurrentProcEvent;

        public enum ProcessingType
        {
            Full,
            FillProducts,
            CheckUpdates,
            FillProductsAndUpds,
            RecheckProductsDatas
        }

        private bool _isStopped;
        public bool IsStopped
        {
            get { return _isStopped; }
            set { _isStopped = value; }
        }


        //Первоначальное заполнение базы данных
        public ProcessingResult GetData(ProcessingType fillType)
        {
            try
            {
                DataBase.Instance.Dataset.AcceptChanges();
                IsStopped = false;

                switch (fillType)
                {
                    case ProcessingType.CheckUpdates:
                        RecheckProducts();
                        return CheckAndFillSelectedItems();
                    case ProcessingType.FillProducts:
                        return FillProductTable();
                    case ProcessingType.RecheckProductsDatas:
                        return RecheckProducts();
                }

                return ProcessingResult.Error;

            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Error in GetData (DataManager) " + ex.Message + " " + ex.StackTrace);
                return ProcessingResult.Error;
            }
        }

        //Заполнение таблицы Products
        private ProcessingResult FillProductTable()
        {
            List<ProductInfo> _newProductsInfo = Helpers.GetProductsInfoFromMainPage(SettingsManager.TITLE_HREF_V8, "project");

            foreach (ProductInfo product in _newProductsInfo)
            {
                DataBase.Instance.AddNewProductRow(product.ProductId, product.ProductName);
            }

            return ProcessingResult.Complete;
        }

        //Получение файлов конфигурации
        private ProcessingResult CheckVersionOutterFiles(string id, string version, string href)
        {

            List<LinkFileInfo> files = Helpers.GetFilesDiscFromHttp(new Uri(SettingsManager.HREF_V8 + href));

            foreach (LinkFileInfo item in files)
            {
                DataBase.Instance.AddContent(id, version, item.FileHttpPath, item.FileName, item.FileDiscription, DataBase.ContentType.VersionFile);
            }

            
            return ProcessingResult.Complete;
        }

        //Получение файлов продукта
        private ProcessingResult CheckProductOutterFiles(ProductInfo productInfo)
        {

            List<LinkFileInfo> files = Helpers.GetFilesDiscFromHttp(new Uri(SettingsManager.HREF_V8 + productInfo.ProductHref));

            foreach (LinkFileInfo item in files)
            {
                DataBase.Instance.AddExtrafiles(productInfo.ProductId, item.FileHttpPath, item.FileName, item.FileDiscription);
                DataBase.Instance.AddHistory(productInfo.ProductId, productInfo.ProductName, DateTime.Now, DataBase.ProductUpdateEvent.ProductContent, item.FileDiscription, DateTime.Now.ToShortDateString());
            }


            return ProcessingResult.Complete;

            //try
            //{
            //    string page = String.Empty;

            //    if (DownloadManager.Instance.DownloadPage(new Uri(Properties.Settings.Default.Web + productInfo.ProductHREF), ref page) == false)
            //    {
            //        return ProcessingResult.Error;
            //    }

            //    //Проверка внешних файлов текущей версии продукта
            //    foreach (var it in HTMLParser.GetHREFSWithPattern(page, "Extrafiles", "getprojectfiles", "/RO/", "BUDGRO"))
            //    {
            //        DataBase.Instance.AddExtrafiles(productInfo.ProductID, it.Key, it.Value);
            //        DataBase.Instance.AddHistory(productInfo.ProductID, productInfo.ProductName, DateTime.Now, DataBase.ProductUpdateEvent.ProductContent, it.Value, DateTime.Now.ToShortDateString());
            //    }

            //    return ProcessingResult.Complete;
            //}
            //catch (Exception ex)
            //{
            //    Logger.LogWriter.Instance.WriteToLog("Error checking outter files " + productInfo.ProductName + " " + ex.StackTrace);
            //    return ProcessingResult.Error;
            //}



        }

        //Проверка изменений по продукту. Сверка новых данных со строкой таблицы Products
        private ProcessingResult CheckProductChanges(DataRow ProductRow, ProductInfo founded)
        {
            bool HasChanges = false;

            try
            {
                //Проверка изменения актуальной версии                
                if (founded.Version != ProductRow["Version"].ToString()
                    && founded.Version != String.Empty
                    && founded.ProductName == ProductRow["Name"].ToString())
                {
                    FillUpdateContentTables(ProductRow["ID"].ToString(), ProductRow["Name"].ToString(), founded.Version, founded.VersionDate, founded.VersionHref, DataBase.ProductUpdateEvent.Release);
                    HasChanges = true;
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Error checking 'actual'" + founded.ProductName + " " + ex.Message + " " + ex.StackTrace);
                return ProcessingResult.Error;
            }


            try
            {
                if (founded.PlanVersion != ProductRow["PlanVersion"].ToString() && founded.ProductName == ProductRow["Name"].ToString())
                {
                    DataBase.Instance.AddHistory(ProductRow["ID"].ToString(), ProductRow["Name"].ToString(), DateTime.Now, DataBase.ProductUpdateEvent.Plan, founded.PlanVersion, founded.PlanDate);
                    HasChanges = true;
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Error checking 'plan'" + founded.ProductName + " " + ex.Message + " " + ex.StackTrace);
                return ProcessingResult.Error;
            }

            try
            {
                //Проверка изменения версии для ознакомления
                // Logger.LogWriter.Instance.WriteToLog("Review ");
                if (founded.ReviewVersion != ProductRow["ReviewVersion"].ToString()
                    && founded.ReviewVersion != String.Empty
                    && founded.ProductName == ProductRow["Name"].ToString())
                {
                    DataBase.Instance.AddHistory(ProductRow["ID"].ToString(), ProductRow["Name"].ToString(), DateTime.Now, DataBase.ProductUpdateEvent.ReviewVersion, founded.ReviewVersion, founded.ReviewDate);
                    //DataBase.Instance.AddContent(ProductRow["ID"].ToString(), founded.ReviewVersion, founded.ReviewHref, "Версия для ознакомления", DataBase.ContentType.VersionFile);
                    FillUpdateContentTables(ProductRow["ID"].ToString(), ProductRow["Name"].ToString(), founded.ReviewVersion, founded.ReviewDate, founded.ReviewHref, DataBase.ProductUpdateEvent.ReviewVersion);
                    HasChanges = true;
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Error checking 'review'" + founded.ProductName + " " + ex.Message + " " + ex.StackTrace);
                return ProcessingResult.Error;
            }


            try
            {
                if (HasChanges)
                    DataBase.Instance.ChangeProductRow(ProductRow, founded);
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Error on changing product row" + founded.ProductName + " " + ex.Message + " " + ex.StackTrace);
                return ProcessingResult.Error;
            }


            return ProcessingResult.Complete;
        }

        //Проверка изменений и заполнение данных по выбранным продуктам в таблице Products
        private ProcessingResult CheckAndFillSelectedItems()
        {
            //Получение титульной страницы сайта с данными о последних изменениях
            List<ProductInfo> newProductsInfo = Helpers.GetProductsInfoFromMainPage(SettingsManager.TITLE_HREF_V8, "project");

            if (newProductsInfo.Count == 0)
                return ProcessingResult.Complete;

            //Обход базы данных по выбранным продуктам
            foreach (var dataBaseItem in DataBase.Instance.Products.Select("Check = true"))
            {
                //Уведомление подписчиков от проверяемом продукте
                if (CurrentProcEvent != null)
                    CurrentProcEvent(this, new CurrentProcEventHandler(dataBaseItem["Name"].ToString()));

                //Поиск продукта в списке
                ProductInfo found = newProductsInfo.Find(la => la.ProductId == dataBaseItem["ID"].ToString());

                //Если не найдено - переход к следующему значению
                if (found.ProductId == null)
                {
                  //  DataBase.Instance.AddProductRow(found.ProductId, found.ProductName, found.Version, found.VersionDate, found.ProductHref, found.PlanVersion, found.PlanDate, true, false);
                    continue;
                }

                //Проверка внешних файлов ПРОДУКТА
                if (CheckProductOutterFiles(found) == ProcessingResult.Error)
                    return ProcessingResult.Error;
                
                //Проверка изенений и запись изменений
                CheckProductChanges(dataBaseItem, found);

                if (IsStopped)
                {
                    DataBase.Instance.Dataset.RejectChanges();
                    IsStopped = false;
                    return ProcessingResult.Stop;
                }


            }

            return ProcessingResult.Complete;
        }

        private ProcessingResult RecheckProducts()
        {
            //Получение титульной страницы сайта с данными о последних изменениях
            List<ProductInfo> newProductsInfo = Helpers.GetProductsInfoFromMainPage(SettingsManager.TITLE_HREF_V8, "project");

            if (newProductsInfo.Count == 0)
                return ProcessingResult.Complete;

            //Обход базы данных по выбранным продуктам
            foreach (DataRow dataBaseItem in DataBase.Instance.Products.Rows)
            {
                //Уведомление подписчиков от проверяемом продукте
                if (CurrentProcEvent != null)
                    CurrentProcEvent(this, new CurrentProcEventHandler(dataBaseItem["Name"].ToString()));

                //Поиск продукта в списке
                ProductInfo found = newProductsInfo.Find(la => la.ProductId == dataBaseItem["ID"].ToString());
                
                
                if (found.ProductId == null)
                {
                  //  ChangeProductAvailableFlag(dataBaseItem, false);
                    dataBaseItem["Available"] = false;
                    dataBaseItem["Check"] = true;
                    DataBase.Instance.AddHistory(dataBaseItem["ID"].ToString(), dataBaseItem["Name"].ToString(), DateTime.Now, DataBase.ProductUpdateEvent.PruductIsUnavailable, "", "");
                }
                
            }
            
            //Обход базы данных по выбранным продуктам
            foreach (var serverProduct in newProductsInfo)
            {
                //Уведомление подписчиков от проверяемом продукте
                if (CurrentProcEvent != null)
                    CurrentProcEvent(this, new CurrentProcEventHandler(serverProduct.ProductName));

                //Поиск продукта в списке
                DataRow FoundRow = DataBase.Instance.Products.Rows.Find(serverProduct.ProductId);


                if (FoundRow == null)
                {
                    var newProductRow = DataBase.Instance.AddProductRow(serverProduct.ProductId, serverProduct.ProductName, serverProduct.Version, serverProduct.VersionDate, serverProduct.ProductHref, serverProduct.PlanVersion, serverProduct.PlanDate, true, true);
                   DataBase.Instance.AddHistory(newProductRow["ID"].ToString(), newProductRow["Name"].ToString(), DateTime.Now, DataBase.ProductUpdateEvent.NewProductAvailable, "", "");
                }
                else //TODO
                {
                 //   DataBase.Instance.AddHistory(newProductRow["ID"].ToString(), newProductRow["Name"].ToString(), DateTime.Now, DataBase.ProductUpdateEvent.NewProductAvailable, "", "");
                }

            }

            return ProcessingResult.Complete;
        }


        //Добавляет данные в таб. Updates - заполняет данные о внешних файла конф. (таб Content) - Заполняет таб. History 
        private void FillUpdateContentTables(string id, string name, string version, string versDate, string verHref, DataBase.ProductUpdateEvent eventType)
        {
            DataBase.Instance.AddUpdatesInfo(id, version, verHref);

            CheckVersionOutterFiles(id, version, verHref);

            DataBase.Instance.AddHistory(id, name,
                DateTime.Now, eventType, version, versDate);
        }
    }
}
