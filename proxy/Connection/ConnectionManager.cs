﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using proxy.Configs;
using proxy.Logger;

namespace proxy.Connection
{
    /// <summary>
    /// Description of ConnectionManager
    /// </summary>
    public sealed class ConnectionManager : MessagerComponent
    {
        public event EventHandler<OnChangeConnectionStatusHandler> OnChangeConnectionStatus;

        const int RecconectionAttemps = 4;
        const int SleepRecconectTime = 10000;
        const int MinSleepInterval = 10000;
        const int MinAttemptsNum = 1;

        #region MyRegion

        private Uri _cookieHostname;
        public Uri CookieHostname
        {
            get { return _cookieHostname; }
            set { _cookieHostname = value; }
        }
   
        private CookieContainer _cookieContainer;
        public  CookieContainer CookieContainer
        {
            get { return  _cookieContainer ?? new CookieContainer(); }
            private set { _cookieContainer = value; }
        }

        private ConnectionStatus _status;
        public  ConnectionStatus Status
        {
            get {
                if (CheckConnection())
                    return Connection.ConnectionStatus.Connected;
                return Connection.ConnectionStatus.NoConnection;
                ; }
            private set { _status = value; }
        }


        private HttpWebRequest _cWebRequest;
        private HttpWebResponse _cWebResponse; 

        public ConnectionResult ConnectionStatus;
        
        #endregion

        private static ConnectionManager _instance = new ConnectionManager();

        public static ConnectionManager Instance
        {
            get
            {
                return _instance;
            }
        }

        private ConnectionManager()
        {
            SetStatus(Connection.ConnectionStatus.NoConnection,  "");         
        }


        private ConnectionResult Connect()
        {
            try
            {
               string login = SettingsManager.GetLogin();
             string password = SettingsManager.GetPassword();

                HttpWebRequest webRequest1 = (HttpWebRequest)WebRequest.Create(new Uri("https://login.1c.ru/login"));
            webRequest1.UserAgent = "1C+Enterprise/8.3";
            webRequest1.KeepAlive = true;
            //  webRequest1.Headers.Add("Proxy-Authorization", "NTLM TlRMTVNTUAABAAAAt7II4gkACQAuAAAABgAGACgAAAAGA4AlAAAAD0RNVkxBRFBMQVRBTi1WTg==");

            HttpWebRequest webRequest2 = (HttpWebRequest)WebRequest.Create(new Uri("https://login.1c.ru/login"));
            webRequest2.UserAgent = "User-Agent: 1C+Enterprise/8.3";
            webRequest2.KeepAlive = true;
            webRequest2.ContentType = "application/x-www-form-urlencoded";

            HttpWebResponse WebResponse = (HttpWebResponse)webRequest1.GetResponse();
            string str = "";
            string jsessionid1 = "";
            using (Stream stream = WebResponse.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    str = reader.ReadToEnd();
                    reader.Close();
                        jsessionid1 = WebResponse.Headers["Set-Cookie"].Substring(0,
                                WebResponse.Headers["Set-Cookie"].IndexOf(";"));
                 //   jsessionid1 = WebResponse.Headers["Set-Cookie"].Substring(92, 36);
                    //  WebResponse.Close();
                }
            }

            string inviteCode = "inviteCode=";
            GetInviteCode(str, ref inviteCode);
            inviteCode += "&username=" + login + "&password=" + password;

            webRequest2.Headers.Add("Cookie", jsessionid1);
            webRequest2.Method = "Post";
            webRequest2.Referer = "https://login.1c.ru/login";
            webRequest2.Accept = "text/html, application/xhtml+xml, */*";
            webRequest2.KeepAlive = true;
            webRequest2.Headers.Add("DNT", "1");
            webRequest2.Headers.Add("Accept-Encoding", "gzip, deflate");
            webRequest2.Headers.Add("Cookie", "client_language=ru_RU; i18next=ru-RU; SESSION=" + jsessionid1);

            //webRequest2.Headers.Add("Cookie", JSESSIONID1);
            webRequest2.Method = "Post";
            //        webRequest2.Accept = "*/*";
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(inviteCode);
            using (Stream newStream = webRequest2.GetRequestStream())
            {
                newStream.Write(data, 0, data.Length);
                newStream.Close();
            }

            webRequest2.AllowAutoRedirect = false;

            WebResponse = (HttpWebResponse)webRequest2.GetResponse();

            //onec_security = ОтветHTTP.Заголовки.Получить("Set-Cookie");
            //Поз = Найти(onec_security, "onec_security");
            //onec_security = Сред(onec_security, Поз);
            //Поз = Найти(onec_security, ";");
            //onec_security = Лев(onec_security, Поз - 1);

            string onecSecurity = WebResponse.Headers["Set-Cookie"];
            int pos = onecSecurity.IndexOf("onec_security", StringComparison.Ordinal);
            string onec = onecSecurity.Substring(pos);
            pos = onec.IndexOf(";", StringComparison.Ordinal);
            onecSecurity = onec.Substring(0, pos);

            webRequest1.UserAgent = "1C+Enterprise/8.3";
            webRequest1.KeepAlive = true;

            webRequest1 = (HttpWebRequest)WebRequest.Create(WebResponse.Headers["Location"]);
            webRequest1.Headers.Add("Cookie", "client_language=ru_RU; i18next=ru-RU; SESSION=" + jsessionid1 + "; " + onecSecurity);
            //    webRequest1.AllowAutoRedirect = false;
            WebResponse = (HttpWebResponse)webRequest1.GetResponse();

            //webRequest1 = (HttpWebRequest)WebRequest.Create(WebResponse.Headers["Location"]);
            //webRequest1.Headers.Add("Cookie", JSESSIONID1 + "; " + onec_security);

            //WebResponse = (HttpWebResponse)webRequest1.GetResponse();

            //webRequest1 = (HttpWebRequest)WebRequest.Create(WebResponse.Headers["Location"]);
            //webRequest1.Headers.Add("Cookie", JSESSIONID1 + "; " + onec_security);

            //WebResponse = (HttpWebResponse)webRequest1.GetResponse();
                /////////////////////////////////////////////
            webRequest1 = (HttpWebRequest)WebRequest.Create(new Uri("https://releases.1c.ru/total/"));
            //   webRequest1.AllowAutoRedirect = false;
            webRequest1.UserAgent = "1C+Enterprise/8.3";
            webRequest1.KeepAlive = true;
            webRequest1.AllowAutoRedirect = false;
            webRequest1.Headers.Add("Cookie", "client_language=ru_RU; i18next=ru-RU; SESSION=" + jsessionid1 + "; " + onecSecurity);

            try
            {
                WebResponse = (HttpWebResponse)webRequest1.GetResponse();
            }
            catch (Exception)
            {

                return ConnectionResult.Connected;
            }


                var locationHeaderValue = "";
            //string JSESSIONID2 = WebResponse.Headers["Set-cookie"].Substring(0, WebResponse.Headers["Set-cookie"].IndexOf(";"));
            string jsessionid2;

            try
            {
                using (var webResponse = (HttpWebResponse)webRequest1.GetResponse())
                {
                    jsessionid2 = webResponse.Headers["Set-cookie"].Substring(0,
                        webResponse.Headers["Set-cookie"].IndexOf(";", StringComparison.Ordinal));
                    locationHeaderValue = webResponse.Headers["Location"];
                }
            }
            catch (Exception)
            {
                return ConnectionResult.WrongLoginPassword;
            }
            //      WebResponse = (HttpWebResponse)webRequest1.GetResponse();


            webRequest1 = (HttpWebRequest)WebRequest.Create(locationHeaderValue);
            webRequest1.UserAgent = "1C+Enterprise/8.3";
            webRequest1.KeepAlive = true;
            webRequest1.AllowAutoRedirect = false;
            webRequest1.Headers.Add("Cookie", "client_language=ru_RU; i18next=ru-RU; SESSION=" + jsessionid1 + "; " + onecSecurity);

            WebResponse = (HttpWebResponse)webRequest1.GetResponse();



            webRequest1 = (HttpWebRequest)WebRequest.Create(WebResponse.Headers["Location"]);
            webRequest1.UserAgent = "1C+Enterprise/8.3";
            webRequest1.KeepAlive = true;
            webRequest1.Headers.Add("Cookie", jsessionid2);
            webRequest1.AllowAutoRedirect = false;

            WebResponse = (HttpWebResponse)webRequest1.GetResponse();
           //string JSESSIONID3 = WebResponse.Headers["Set-cookie"].Substring(0, WebResponse.Headers["Set-cookie"].IndexOf(";"));


            webRequest1 = (HttpWebRequest)WebRequest.Create(new Uri("https://releases.1c.ru/total"));
            //webRequest1.Headers.Add("Cookie", JSESSIONID3);
            //   webRequest1.AllowAutoRedirect = false;
            WebResponse = (HttpWebResponse)webRequest1.GetResponse();

            SettingsManager.JSONCODE = jsessionid2;
                       

            return ConnectionResult.Connected;
            }
            catch (Exception e)
            {
                LogWriter.Instance.WriteToLog(e.Message, LogLevel.Error);
                return ConnectionResult.ServerError;
            }
        }

        private void GetInviteCode(string str, ref string inviteCode)
        {

            var document = new HtmlDocument();

            document.LoadHtml(str);

            //var node = document.DocumentNode.SelectSingleNode("//span[@id='something']");
            var nodes = document.DocumentNode.SelectNodes("//input");

            foreach (var node in nodes)
            {
                foreach (var atr in node.Attributes)
                {
                    if (atr.Value == "lt")
                    {
                        inviteCode += "&lt=" + node.Attributes[2].Value;
                    }
                    if (atr.Value == "execution")
                    {
                        inviteCode += "&execution=" + node.Attributes[2].Value;
                    }
                    if (atr.Value == "_eventId")
                    {
                        inviteCode += "&_eventId=" + node.Attributes[2].Value;
                        //            found = true;
                    }
                }
            }


        }

        private void SetStatus(ConnectionStatus connectionStatus, string info)
        {
            Status = connectionStatus;
            OnRaiseCustomEvent(new OnChangeConnectionStatusHandler(connectionStatus, info));

        }

        private bool CheckConnection()
        {
            try
            {

                _cWebRequest = (HttpWebRequest)WebRequest.Create(SettingsManager.TITLE_HREF_V8);
                _cWebRequest.AllowAutoRedirect = false;
                _cWebRequest.Method = "GET";
                _cWebRequest.Headers.Add(HttpRequestHeader.Cookie, Instance.CookieContainer.GetCookieHeader(new Uri(SettingsManager.HREF_V8)));

                _cWebResponse = (HttpWebResponse)_cWebRequest.GetResponse();

                if (_cWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
            }
            catch (Exception exception)
            {
                LogWriter.Instance.WriteToLog(exception.Message);
                return false;
            }

            return false;
        }

        public bool SetConnection()
        {
            if (CheckConnection())
            {
                SetStatus(Connection.ConnectionStatus.Connected, DateTime.Now.ToLongTimeString());
                return true;
            }

            for (int i = 1; i < SettingsManager.RECON_BATTEMPTS; i++)
            {
                SetStatus(Connection.ConnectionStatus.Reconnecting, "(попытка " + i + ")");
                var connResult = Connect();

                if (connResult == ConnectionResult.WrongLoginPassword)
                {
                    SetStatus(Connection.ConnectionStatus.WrongLoginPassword, DateTime.Now.ToLongTimeString());
                    return false;
                }

                if (connResult == ConnectionResult.ServerError)
                {
                    SetStatus(Connection.ConnectionStatus.NoConnection, DateTime.Now.ToLongTimeString());
                    return false;
                }

                if (connResult == ConnectionResult.Connected)
                {                   
                    SetStatus(Connection.ConnectionStatus.Connected, DateTime.Now.ToLongTimeString());
                    return true;
                }
                if (i >= RecconectionAttemps)
                    Thread.Sleep(SettingsManager.RECON_BINTERVAL);
                else
                    Thread.Sleep(SettingsManager.RECON_SINTERVAL);
            }

            if (ConnectionStatus != ConnectionResult.Connected)
            {
                Thread.Sleep(SleepRecconectTime);
                SetStatus(Connection.ConnectionStatus.ConnectionError, DateTime.Now.ToString());
             
                return false;
            }

            SetStatus(Connection.ConnectionStatus.ConnectionError, DateTime.Now.ToString());

            return false;
        }

        public bool SetConnection(int attemptsNum, int sleepInterval)
        {
            if (attemptsNum < MinAttemptsNum)
            {
                LogWriter.Instance.WriteToLog("Not valid reconnection attempt number = " + attemptsNum + " 'attemptsNum' set to " + MinAttemptsNum);
                attemptsNum = 1;
            }
            if (sleepInterval < 1 || sleepInterval < MinSleepInterval)
            {
                LogWriter.Instance.WriteToLog("Not valid sleep interval (ms) = " + sleepInterval + " 'sleepInterval' set to " + MinSleepInterval);               
            }

            if (CheckConnection())
            {
                LogWriter.Instance.WriteToLog("Соединение установлено");
                SetStatus(Connection.ConnectionStatus.Connected, DateTime.Now.ToLongTimeString());
                return true;
            }

            for (int i = 1; i <= attemptsNum; i++)
            {
                LogWriter.Instance.WriteToLog("Попытка соединения " + i);
                SetStatus(Connection.ConnectionStatus.Reconnecting, "(попытка " + i + ")");

                if (Connect() == ConnectionResult.WrongLoginPassword)
                {
                    LogWriter.Instance.WriteToLog("Неверный логин или пароль");
                    SetStatus(Connection.ConnectionStatus.WrongLoginPassword, DateTime.Now.ToLongTimeString());
                    return false;
                }

                if (Connect() == ConnectionResult.Connected)
                {
                    LogWriter.Instance.WriteToLog("Соединение установлено");
                    SetStatus(Connection.ConnectionStatus.Connected, DateTime.Now.ToLongTimeString());
                    return true;
                }

                Thread.Sleep(sleepInterval);
            }

            if (ConnectionStatus != ConnectionResult.Connected)
            {
                LogWriter.Instance.WriteToLog("Ошибка соединения с сервером");
                SetStatus(Connection.ConnectionStatus.ConnectionError, DateTime.Now.ToString());
                return false;
            }         

            return false;            
        }

        public  WebResponse GetWebResponse(Uri link, string cookies)
        {
            WebRequest webReq = WebRequest.Create(link);
            webReq.Headers.Add("Cookie", SettingsManager.JSONCODE);
            WebResponse webResp = webReq.GetResponse();

            return webResp;
        }

        public string GetResponseString(WebResponse webResponse, Encoding encoding)
        {
            string result;
            using (Stream strResp = webResponse.GetResponseStream())
            {
                using (var strLocal = new StreamReader(strResp, encoding))
                {
                    result = strLocal.ReadToEnd();
                  
                    if (strResp != null) 
                        strResp.Close();

                    strLocal.Close();
                }
            }


            return result;
        }


        private void OnRaiseCustomEvent(OnChangeConnectionStatusHandler e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<OnChangeConnectionStatusHandler> handler = OnChangeConnectionStatus;

            // Event will be null if there are no subscribers
            if (handler != null)
            {
                // Format the string to send inside the CustomEventArgs parameter

                e.ConnectionStatus = e.ConnectionStatus;
                e.Info = e.Info;
                //  MessagesStack.AddMessage(e.Message);
                // Use the () operator to raise the event.
                handler(this, e);
            }
        }



    }
}
