namespace proxy.Connection
{
    public enum ConnectionStatus
    {
        Connecting,
        Reconnecting,
        Busy,
        Connected,
        ConnectionError,
        WrongLoginPassword,
        NoConnection
    }
}