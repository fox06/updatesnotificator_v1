namespace proxy.Connection
{
    public enum ConnectionResult
    {
        NoInternetConnection,
        WrongLoginPassword,
        ServerNotFound,
        ServerError,
        Connected
    }
}