﻿using System;

namespace proxy.Connection
{
    public class OnChangeConnectionStatusHandler : EventArgs
    {
        public ConnectionStatus ConnectionStatus { get; set; }

        public string Info { get; set; }
        
        public OnChangeConnectionStatusHandler(ConnectionStatus connectionStatus, string addInfo = "")
        {
            Info = addInfo;
            ConnectionStatus = connectionStatus;
        }
    }
}