﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace proxy
{
    static class HtmlParser
        {



        static string[] _searchPattern = {
           "zip",
           "rar",
           "ftp",
           "downloads"         
        };

           


            //public static Dictionary<string, string> GetHREFS(string text)
            //{
            //    Dictionary<string, string> found = new Dictionary<string, string>();

            //    Regex regex = new Regex(@"(?<=href\=" + Convert.ToChar('"') + ")[^" + Convert.ToChar('"') + "]+?(?=" + Convert.ToChar('"') + ")");

            //    Match match = regex.Match(text);


            //    while (match.Success)
            //    {
            //        if (!found.ContainsKey(match.Value))
            //            found.Add(match.Value);

            //        match = match.NextMatch();
            //    }

            //    return found;
            //}

        public static Dictionary<string, string> GetHrefsWithPattern(string text, params string[] searchPattern)
        {
            Dictionary<string, string> foundDic = new Dictionary<string, string>();

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(text);

            HtmlNodeCollection s = doc.DocumentNode.SelectNodes("//a[@href]");
            if (s == null)
                return foundDic;


            foreach (HtmlNode link in s)
            {
                HtmlAttribute attrib = link.Attributes["href"];

                if (!foundDic.ContainsKey(attrib.Value.Trim()))
                    if (ContainsPattern(attrib.Value.Trim(), searchPattern))
                        foundDic.Add(attrib.Value.Trim(), ClearString(link.InnerText));

            }

            doc = null;
            return foundDic;

        }

            public static string GetIdFromHref(string href)
            {
                //Поиск всех символов после "id=" (project.jsp?id=AddCompDB2hotfix)
                Regex regex = new Regex(@"(?<=project/)\w+");
                //  Regex regex = new Regex(@"(?<=href\=" + Convert.ToChar('"') + ")[^" + Convert.ToChar('"') + "]+?(?=" + Convert.ToChar('"') + ")");
                Match match = regex.Match(href);

                return match.Value;
            }

            public static string GetHrefFrom(string href)
            {
                //Поиск всех символов после "id=" (project.jsp?id=AddCompDB2hotfix)
               // Regex regex = new Regex(@"(?<=(id=))\w+");
                  Regex regex = new Regex(@"(?<=href\=" + Convert.ToChar('"') + ")[^" + Convert.ToChar('"') + "]+?(?=" + Convert.ToChar('"') + ")");
                Match match = regex.Match(href);

                return match.Value;
            }

            private static bool ContainsPattern(string text, string[] searchPattern)
            {   

                for (int i = 0; i < searchPattern.Length; i++)
                {
                    if (text.Contains(searchPattern[i]))
                        return true;
                }

                return false;
            }

            public static string ClearString(string s)
            {
                string ns = s.Trim().Replace("&nbsp;", "");
                ns = ns.Replace("&quot;", "");
                return RemoveTagsAndTrim(ns);

            }

            public static string RemoveTagsAndTrim(string text)
            {
                return Regex.Replace(text.Trim(), @"\t|\n|\r|&nbsp;", "");
            }
                      
        }
    }
