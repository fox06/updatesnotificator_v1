﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using proxy;
using proxy.Properties;

namespace DataGridViewCustomCell
{     
    ///
    /// Column which will be attached to the DataGridView.
    ///
    public class ImageStatusColumn : DataGridViewColumn
    {

        public DataBase.ProductUpdateEvent StatusImage;

        public ImageStatusColumn()
            : base(new StatusCell())
        {
        }
        private DataBase.ProductUpdateEvent m_DefaultStatus = DataBase.ProductUpdateEvent.Plan;

        public DataBase.ProductUpdateEvent DefaultStatus
        {
            get { return m_DefaultStatus; }
            set { m_DefaultStatus = value; }
        }

        public override object Clone()
        {
            ImageStatusColumn col = base.Clone() as ImageStatusColumn;
            col.DefaultStatus = m_DefaultStatus;
            return col;
        }

        public override DataGridViewCell CellTemplate
        {
            get { return base.CellTemplate; }
            set
            {
                if ((value == null) || !(value is StatusCell))
                {
                    throw new ArgumentException(
                        "Invalid cell type, StatusColumns can only contain StatusCells");
                }
            }
        }
    }

    ///
    /// Cell which is attached to the ImageStatusColumn.
    ///
    public class StatusCell : DataGridViewImageCell
    {
        public StatusCell()
        {
            ImageLayout = DataGridViewImageCellLayout.Zoom;
        }

        protected override object GetFormattedValue(object value,
           int rowIndex, ref DataGridViewCellStyle cellStyle,
           TypeConverter valueTypeConverter,
           TypeConverter formattedValueTypeConverter,
           DataGridViewDataErrorContexts context)
        {
            
            Bitmap resource = Resources.content_new;
            DataBase.ProductUpdateEvent status = DataBase.ProductUpdateEvent.Plan;
            // Try to get the default value from the containing column
          //  ImageStatusColumn owningCol = OwningColumn as ImageStatusColumn;
          //  if (owningCol != null)
          //  {
          //      status = owningCol.DefaultStatus;
          //  }
            if (value is DataBase.ProductUpdateEvent || value is int)
            {
                status = (DataBase.ProductUpdateEvent)value;
            }
            switch (status)
            {
                case DataBase.ProductUpdateEvent.Plan:
                    resource = Resources.info_new;
                    break;
                case DataBase.ProductUpdateEvent.ProductContent:
                    resource = Resources.content_new;                     
                    break;
                case DataBase.ProductUpdateEvent.VersionContent:
                    resource = Resources.content_new;
                    break;
                case DataBase.ProductUpdateEvent.Release:
                    resource = Resources.update_new;
                    break;
                case DataBase.ProductUpdateEvent.ReviewVersion:
                    resource = Resources.review_new;
                    break;
                default:
                    break;
            }
            cellStyle.Alignment = DataGridViewContentAlignment.TopCenter;
            return resource;
        }
    }


    public class DownloadStatusColumn : DataGridViewColumn
    {

        public DataBase.ProductUpdateEvent StatusImage;

        public DownloadStatusColumn()
            : base(new DownloadStatusCell())
        {
        }
        private DataBase.DownloadProgress m_DefaultStatus = DataBase.DownloadProgress.Waiting;

        public DataBase.DownloadProgress DefaultStatus
        {
            get { return m_DefaultStatus; }
            set { m_DefaultStatus = value; }
        }

        public override object Clone()
        {
            DownloadStatusColumn col = base.Clone() as DownloadStatusColumn;
            col.DefaultStatus = m_DefaultStatus;
            return col;
        }

        public override DataGridViewCell CellTemplate
        {
            get { return base.CellTemplate; }
            set
            {
                if ((value == null) || !(value is DownloadStatusCell))
                {
                    throw new ArgumentException(
                        "Invalid cell type, StatusColumns can only contain StatusCells");
                }
            }
        }
    }

    ///
    /// Cell which is attached to the ImageStatusColumn.
    ///
    public class DownloadStatusCell : DataGridViewTextBoxCell
    {
        protected override object GetFormattedValue(object value,
           int rowIndex, ref DataGridViewCellStyle cellStyle,
           TypeConverter valueTypeConverter,
           TypeConverter formattedValueTypeConverter,
           DataGridViewDataErrorContexts context)
        {

            string resource = String.Empty;
            DataBase.DownloadProgress status = DataBase.DownloadProgress.Waiting;
            // Try to get the default value from the containing column
            DownloadStatusColumn owningCol = OwningColumn as DownloadStatusColumn;
            if (owningCol != null)
            {
                status = owningCol.DefaultStatus;
            }
            if (value is DataBase.ProductUpdateEvent || value is int)
            {
                status = (DataBase.DownloadProgress)value;
            }
            switch (status)
            {
                case DataBase.DownloadProgress.Waiting:
                    resource = "Не загружен";
                    break;
                case DataBase.DownloadProgress.Completed:
                    resource = "Загружен";
                    break;
                case DataBase.DownloadProgress.Downloading:
                    resource = "Загрузка...";
                    break;
                case DataBase.DownloadProgress.Error:
                    resource = "Ошибка загрузки";
                    break;
                case DataBase.DownloadProgress.NoFiles:
                    resource = "Нет файлов для загрузки";
                    break;
                default:
                    break;
            }
            cellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            return resource;
        }
    }



}