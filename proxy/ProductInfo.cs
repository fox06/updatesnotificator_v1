namespace proxy
{
    public struct ProductInfo
    {
        public string ProductId;
        public string ProductName;
        public string ProductHref;
        public string Version;
        public string VersionHref;
        public string VersionDate;
        public string PlanVersion;
        public string PlanDate;
        public string ReviewVersion;
        public string ReviewDate;
        public string ReviewHref;
    }
}