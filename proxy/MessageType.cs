namespace proxy
{
    public enum MessageType
    {
        Info = 0,
        Warning = 1,
        Error = 2,
        Critical = 3,
        Important = 4,
        Update = 5,
        ConnectionStatus = 6
    }
}