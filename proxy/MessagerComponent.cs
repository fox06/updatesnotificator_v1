using System;

namespace proxy
{
    public abstract class MessagerComponent
    {
        public string Message { get; set; }

        public MessageType MessageType { get; set; }

        public event EventHandler<CustomEventArgs> RaiseCustomEvent;

        // public abstract void Draw();

        //The event-invoking method that derived classes can override.
        protected virtual void OnRaiseCustomEvent(CustomEventArgs e)
        {
            EventHandler<CustomEventArgs> handler = RaiseCustomEvent;
            
            if (handler != null)
            {
                e.Message += String.Format(" ({0})", DateTime.Now);
                handler(this, e);
            }
        }
    }
}