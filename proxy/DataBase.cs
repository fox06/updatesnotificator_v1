﻿using System;
using System.Data;
using System.IO;
using proxy.Configs;
using proxy.Logger;

namespace proxy
{
    /// <summary>
    /// Description of SingletonClass1
    /// </summary>
    public sealed class DataBase
    {
        #region Fields
        private static DataBase instance = new DataBase();

        private DataSet dataset;
        public DataSet Dataset { get { return dataset; } set { dataset = value; } }

        private DataTable products;
        public DataTable Products { get { return products; } }

        private DataTable updates;
        public DataTable Updates { get { return updates; } }

        private DataTable content;
        public DataTable Content { get { return content; } }

        private DataTable history;
        public DataTable History { get { return history; } }

        private DataTable extrafiles;
        public DataTable Extrafiles { get { return extrafiles; } }

        public static DataBase Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        [Flags]
        public enum ProductUpdateEvent
        {
            Plan,
            Release,
            VersionContent,
            ProductContent,
            ReviewVersion,
            NewProductAvailable,
            PruductIsUnavailable
        }


        public enum ContentType
        {
            VersionFile,
            VersionOutterFile,
            ConfigOutterFile
        }

        public enum DownloadProgress
        {
            Completed,
            Downloading,
            Error,
            Waiting,
            NoFiles
        }


        private DataBase()
        {

            using (dataset = new DataSet())
            {


                //Таблица конфигураций с свойством available - доступность под тек. пользователем
                products = new DataTable("Products");
                products.Columns.Add(new DataColumn("ID", typeof(string)));
                products.Columns.Add(new DataColumn("Name", typeof(string)));
                products.Columns.Add(new DataColumn("Version", typeof(string)));
                products.Columns.Add(new DataColumn("Date", typeof(string)));
                products.Columns.Add(new DataColumn("href", typeof(string)));
                products.Columns.Add(new DataColumn("PlanVersion", typeof(string)));
                products.Columns.Add(new DataColumn("PlanDate", typeof(string)));
                products.Columns.Add(new DataColumn("ReviewVersion", typeof(string)));
                products.Columns.Add(new DataColumn("ReviewDate", typeof(string)));
                products.Columns.Add(new DataColumn("ReviewHref", typeof(string)));
                products.Columns.Add(new DataColumn("Available", typeof(bool)));
                products.Columns.Add(new DataColumn("Check", typeof(bool)));
                Products.PrimaryKey = new[] { Products.Columns["ID"] };

                //////////////
                updates = new DataTable("Updates");
                updates.Columns.Add(new DataColumn("ID", typeof(string)));
                updates.Columns.Add(new DataColumn("Version", typeof(string)));
                updates.Columns.Add(new DataColumn("href", typeof(string)));
                updates.Columns.Add(new DataColumn("From_version", typeof(string)));
                updates.PrimaryKey = new[] { Updates.Columns["ID"], Updates.Columns["Version"] };

                /////////////
                content = new DataTable("Content");
                content.Columns.Add(new DataColumn("ID", typeof(string)));
                content.Columns.Add(new DataColumn("Version", typeof(string)));
                content.Columns.Add(new DataColumn("href", typeof(string)));
                content.Columns.Add(new DataColumn("filename", typeof(string)));
                content.Columns.Add(new DataColumn("discription", typeof(string)));
                content.Columns.Add(new DataColumn("ContentType", typeof(ContentType)));
                content.Columns["ContentType"].DefaultValue = ContentType.VersionFile;
                //content.Rows.s
                content.PrimaryKey = new[] { content.Columns["ID"], content.Columns["Version"], content.Columns["href"] };

                //////////////////////
                extrafiles = new DataTable("Extrafiles");
                extrafiles.Columns.Add(new DataColumn("ID", typeof(string)));
                extrafiles.Columns.Add(new DataColumn("href", typeof(string)));
                extrafiles.Columns.Add(new DataColumn("filename", typeof(string)));
                extrafiles.Columns.Add(new DataColumn("discription", typeof(string)));
                extrafiles.PrimaryKey = new[] { extrafiles.Columns["ID"], extrafiles.Columns["href"] };

                /////////////
                history = new DataTable("History");

                //  history.Columns[0].AutoIncrement = true;
                //   history.Columns.Add(new DataColumn("Count", typeof(int)));
                history.Columns.Add(new DataColumn("ID", typeof(string)));
                history.Columns.Add(new DataColumn("Name", typeof(string)));
                history.Columns.Add(new DataColumn("Date", typeof(DateTime)));
                history.Columns.Add(new DataColumn("ChangeDisc", typeof(ProductUpdateEvent)));
                history.Columns.Add(new DataColumn("Discription", typeof(string)));
                history.Columns.Add(new DataColumn("EventVersion", typeof(string)));
                history.Columns.Add(new DataColumn("EventDate", typeof(string)));
                history.Columns.Add(new DataColumn("IsDownloaded", typeof(bool)));
                history.Columns.Add(new DataColumn("DownloadProgress", typeof(DownloadProgress)));
                history.Columns["DownloadProgress"].AllowDBNull = false;
                history.Columns["DownloadProgress"].DefaultValue = DownloadProgress.Waiting;
                history.PrimaryKey = new[] { history.Columns["ID"], history.Columns["ChangeDisc"], history.Columns["EventVersion"], history.Columns["EventDate"] };


                /////////////

                try
                {
                    dataset.Tables.Add(Products);
                    dataset.Tables.Add(Updates);
                    dataset.Tables.Add(Content);
                    dataset.Tables.Add(History);
                    dataset.Tables.Add(Extrafiles);

                    dataset.Relations.Add("id_relation", Products.Columns["ID"], Updates.Columns["ID"]);
                    dataset.Relations.Add("id_extrafiles", Products.Columns["ID"], Extrafiles.Columns["ID"]);
                    dataset.Relations.Add("id_version", new[] { Updates.Columns["ID"], Updates.Columns["Version"] }, new[] { Content.Columns["ID"], Content.Columns["Version"] });
                    dataset.Relations.Add("id_prod_history", Products.Columns["ID"], History.Columns["ID"]);
                }
                catch (Exception)
                {
                    LogWriter.Instance.WriteToLog("Ошибка построения структуры базы данных", LogLevel.Critical);
                    throw new Exception("Cannot build data base structure");
                }

            }

        }

        public DataRow AddProductRow(string ID, string Name, string version,
            string release_date, string href,
            string plan_version, string plan_date,
            bool Available = false, bool Check = true)
        {
            DataRow FoundRow = Products.Rows.Find(ID);

            if (FoundRow == null && ID != "")
                FoundRow = dataset.Tables[0].Rows.Add(ID, Name, version, release_date, href, plan_version, plan_date, Available, Check);

            return FoundRow;
        }

        public DataRow AddNewProductRow(string ID, string Name)
        {
            if (ID == String.Empty)
            {
                LogWriter.Instance.WriteToLog("ID продукта должен быть заполнен " + Name, LogLevel.Critical);
                throw new Exception("Product 'ID' must be filled");
            }


            DataRow NewProdRow = Products.NewRow();
            NewProdRow["ID"] = ID;
            NewProdRow["Name"] = Name;
            NewProdRow["Available"] = true;

            Products.Rows.Add(NewProdRow);

            return NewProdRow;
        }


        public void ChangeProductRow(DataRow prodRow, ProductInfo prodInfo, bool Available = true)
        {
            prodRow["Version"] = prodInfo.Version;
            prodRow["Date"] = prodInfo.VersionDate;
            prodRow["PlanVersion"] = prodInfo.PlanVersion;
            prodRow["PlanDate"] = prodInfo.PlanDate;
            prodRow["href"] = prodInfo.ProductHref;
            prodRow["ReviewVersion"] = prodInfo.ReviewVersion;
            prodRow["ReviewDate"] = prodInfo.ReviewDate;
            prodRow["ReviewHref"] = prodInfo.ReviewHref;
            prodRow["Available"] = Available;
        }

        public void ChangeProductAvailableFlag(DataRow prodRow, bool Available = true)
        {
            prodRow["Available"] = Available;
        }

        public void AddUpdatesInfo(string ID, string version, string href)
        {                      
            DataRow FoundRow = Updates.Rows.Find(new[] { ID, version });

            if (FoundRow == null && ID != "")
            {
                try
                {
                    Updates.Rows.Add(ID, version, href);
                }
                catch (Exception e)
                {
                    LogWriter.Instance.WriteToLog(e.Message + "::::::" + e.Source + "::::::" + e.StackTrace);
                    throw new Exception(e.Message);
                }

            }
        }

        public void AddContent(string id, string version, string href, string fileName, string discription, ContentType contentType)
        {
            DataRow FoundRow = Content.Rows.Find(new[] { id, version, href });

            if (FoundRow == null && version != String.Empty)
            {
                try
                {
                    Content.Rows.Add(id, version, href, fileName, discription, contentType);
                }
                catch (Exception)
                {
                    LogWriter.Instance.WriteToLog("Error adding to 'Content' table. " + id + "/" + contentType + "/" + discription + "/" + contentType);
                    //        throw;
                }
            }
            else if (FoundRow != null)
            {
                FoundRow["Version"] = version;
                FoundRow["href"] = href;
                FoundRow["filename"] = fileName;
                FoundRow["discription"] = discription;
                FoundRow["ContentType"] = contentType;
            }
        }

        public void AddExtrafiles(string id, string href, string fileName, string discription)
        {
            DataRow FoundRow = extrafiles.Rows.Find(new[] { id, href });

            if (FoundRow == null)
            {
                try
                {
                    //Console.WriteLine(id + "_" + version + "_" + discription);
                    extrafiles.Rows.Add(id, href, fileName, discription);
                }
                catch (Exception)
                {
                    LogWriter.Instance.WriteToLog("Error adding to 'Extrafiles' table. " + id + "/" + href + "/" + discription);
                }
            }
            else if (FoundRow != null)
            {
                FoundRow["href"] = href;
                FoundRow["filename"] = href;
                FoundRow["discription"] = discription;
            }
        }

        public void AddHistory(string id, string name, DateTime date, ProductUpdateEvent changeDisc, string eventVersion, string eventDate, bool isDownloaded = false)
        {
            DataRow FoundRow = dataset.Tables["History"].Rows.Find(new object[] { id, changeDisc, eventVersion, eventDate });

            if (FoundRow == null && id != String.Empty)
            {
                try
                {
                    DataRow newDr = history.NewRow();
                    newDr[0] = id;
                    newDr[1] = name;
                    newDr[2] = date;
                    newDr[3] = changeDisc;

                    string _discription = String.Empty;

                    switch (changeDisc)
                    {
                        case ProductUpdateEvent.Plan:
                            _discription = String.Format("Планируется версия: {0} ({1})", eventVersion, eventDate);
                            break;
                        case ProductUpdateEvent.Release:
                            _discription = String.Format("Новая версия: {0} ({1})", eventVersion, eventDate);
                            break;
                        case ProductUpdateEvent.ReviewVersion:
                            _discription = String.Format("Версия для ознакомления: {0} ({1})", eventVersion, eventDate);
                            break;
                        case ProductUpdateEvent.ProductContent | ProductUpdateEvent.VersionContent:
                            _discription = eventVersion;
                            break;
                        case ProductUpdateEvent.NewProductAvailable:
                            _discription =  String.Format("Доступен новый продукт");
                            break;
                        case ProductUpdateEvent.PruductIsUnavailable:
                            _discription = String.Format("Продукт более недоступен"); 
                            break;

                        default:
                            break;
                    }

                    newDr[4] = _discription;
                    newDr[5] = eventVersion;
                    newDr[6] = eventDate;
                    newDr[7] = isDownloaded;

                    dataset.Tables["History"].Rows.Add(newDr);
                }
                catch (Exception)
                {
                    LogWriter.Instance.WriteToLog("Error adding to 'History' table. " + id);
                }

            }
            else if (FoundRow != null)
            {
                //  FoundRow["Date"] = date;
                //  FoundRow["ChangeDisc"] = changeDisc;
                //   FoundRow["EventVersion"] = eventVersion;
                //   FoundRow["EventDate"] = eventDate;
                //   FoundRow["Name"] = name;
            }
        }



        public static bool LoadDataBase()
        {
            try
            {
                using (FileStream streamRead = new FileStream(SettingsManager.DB_PATH, FileMode.Open))
                {
                    Instance.dataset.Clear();
                    Instance.dataset.ReadXml(streamRead, XmlReadMode.IgnoreSchema);
                    Instance.dataset.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Ошибка загрузки базы данных:" + ex.Message + "::::::" + ex.Source + "::::::" + ex.StackTrace);
                return false;
                //throw new Exception("Невозможно загрузить базу данных");
            }

            LogWriter.Instance.WriteToLog("База  данных загружена");
            return true;
        }

        public static bool SaveDataBase()
        {
            try
            {
                Instance.Dataset.AcceptChanges();
                using (FileStream streamRead = new FileStream(SettingsManager.DB_PATH, FileMode.Create))
                {
                    Instance.dataset.WriteXml(streamRead);
                }
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Ошибка записи базы данных:" + ex.Message + "::::::" + ex.Source + "::::::" + ex.StackTrace); ;
                return false;
            }

            return true;
        }

    }
}
