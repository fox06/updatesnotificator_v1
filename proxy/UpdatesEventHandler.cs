﻿using System;

namespace proxy
{
    public class UpdatesEventHandler : EventArgs
    {
        public UpdatesStatus UpdatesStatus { get; set; }

        public string Info { get; set; }


        public UpdatesEventHandler(UpdatesStatus updatesStatus, string info = "")
        {
            UpdatesStatus = updatesStatus;
            Info = info;
        }
    }
}


