﻿using System;
using System.Windows.Forms;
using proxy.Common;
using proxy.Properties;

namespace proxy.Configs
{
    public static class SettingsManager
    {
        public const int MSEC_HOUR = 60000;
        public const int RECON_BINTERVAL = 120000;
        public const int RECON_SINTERVAL = 15000;
        public const int RECON_BATTEMPTS = 5;
        public static string JSONCODE = "";

        public const string TITLE_HREF_V8 = "https://releases.1c.ru/total";
        public const string HREF_V8 = "https://releases.1c.ru/";

        
        public static string[] ReconfigMode = new string[4] { "o/X+QD25uQI=", "Ba72ew2axxeCWi386wR67jwRErbBlo+k", "m/ti8PhKMRt/Ss19vMp2gA==", "gm662TmSP7ebNwDmsY/+39K/o73IAc84" };
 
        public const string DATABASEFILE = "db.xml";
        public static readonly string APP_FOLDER_PATH = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string DB_PATH = AppDomain.CurrentDomain.BaseDirectory + DATABASEFILE;
        public static readonly string DEFAULT_UPD_FOLDER = AppDomain.CurrentDomain.BaseDirectory + "\\Updates";
        public static readonly string EXE_PATH = Application.ExecutablePath;

        public const int DOWNLOAD_SLOTS_NUM = 5;

        public static string GetPassword()
        {
            return Helpers.Decrypt(Settings.Default.Password, true, "");
        }

        public static string GetLogin()
        {
            return Settings.Default.Login;
        }

        static SettingsManager()
        {
            if (Settings.Default.UseDefaultUpdFolder)
            {
                Settings.Default.UpdatesFolder = DEFAULT_UPD_FOLDER;
            }
        }
    }
}
