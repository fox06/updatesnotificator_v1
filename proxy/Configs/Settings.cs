﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;

namespace proxy.Properties {
    
    
    // Этот класс позволяет обрабатывать определенные события в классе параметров:
    //  Событие SettingChanging возникает перед изменением значения параметра.
    //  Событие PropertyChanged возникает после изменения значения параметра.
    //  Событие SettingsLoaded возникает после загрузки значений параметров.
    //  Событие SettingsSaving возникает перед сохранением значений параметров.
    public sealed partial class Settings {
             

        public Settings() {

            if (DaysForCheck == null || DaysForCheck.Count < 7 )
            {
                string[] v = { "true", "true", "true", "true", "true", "false", "false" };
                DaysForCheck = new StringCollection();
                DaysForCheck.AddRange(v);
            }

            //this.PropertyValues.
            
            // // Для добавления обработчиков событий для сохранения и изменения параметров раскомментируйте приведенные ниже строки:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
        }
        
        private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e) {
            // Добавьте здесь код для обработки события SettingChangingEvent.
            
        }
        
        private void SettingsSavingEventHandler(object sender, CancelEventArgs e) {
            // Добавьте здесь код для обработки события SettingsSaving.
        }
    }

   
}
