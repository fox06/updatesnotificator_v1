﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using proxy.Common;
using proxy.Configs;
using proxy.Connection;
using proxy.Download;
using proxy.Forms;
using proxy.Logger;
using proxy.Properties;
using Timer = System.Timers.Timer;

namespace proxy
{
    class ProgramManager : MessagerComponent
    {
        public event EventHandler<UpdatesEventHandler> UpdatesEvents;

        private Timer _timer;
        private MainForm _mainForm;

        private DataManager _dataManager;
        internal DataManager DataManager
        {
            get { return _dataManager; }
        }

        public GlobalStates State { get; set; }

        private static ProgramManager instance = new ProgramManager();

        public static ProgramManager Instance
        {
            get
            {
                return instance;
            }
        }

        private ProgramManager()
        {

        }

        public void OnStartApp(string[] args)
        {
            try
            {

                LogWriter.Instance.WriteToLog("Запуск приложения");

                Initializaition();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                _mainForm = new MainForm();

                ConnectionManager.Instance.OnChangeConnectionStatus += _mainForm.OnChangeConnectionStatus;
                UpdatesEvents += _mainForm.OnUpdatesEvent;

                if (Array.Find(args, element => element.StartsWith("/a", StringComparison.Ordinal)) != null)
                {
                    Instance.StartChecking();
                };

            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog(ex.Message + "::::::" + ex.Source + "::::::" + ex.StackTrace);
                OnExitApp();
            }

            Application.Run(_mainForm);
            //  OnExitApp();

            //    Logger.LogWriter.Instance.WriteToLog("Exit");
        }

        public void OnExitApp()
        {
            LogWriter.Instance.WriteToLog("Завершение работы");

            DataBase.SaveDataBase();
            Settings.Default.Save();
            DownloadManager.Instance.Dispose();
            //   DataBase.Instance.Dataset.AcceptChanges();

            Application.Exit();
        }

        public void Initializaition()
        {
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
            ServicePointManager.MaxServicePoints = int.MaxValue;
            ServicePointManager.MaxServicePointIdleTime = 0;

            //Загрузка базы данных
            DataBase.LoadDataBase();

            //Инициализация Менеджера данных
            _dataManager = new DataManager();
            _dataManager.CurrentProcEvent += dataManager_CurrentProcEvent;


            //Инициализация основного таймера
            _timer = new Timer();
            _timer.Elapsed += TimerEvent;
        }

        void dataManager_CurrentProcEvent(object sender, CurrentProcEventHandler e)
        {
            if (UpdatesEvents != null)
                UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.AddiditionInfo, e.CurrentProcObj));
        }

        private bool CheckFileDirectoryStructure()
        {
            try
            {
                if (!Directory.Exists(Helpers.GetUpdatesFolder()))
                    Directory.CreateDirectory(Helpers.GetUpdatesFolder());

                if (!File.Exists(SettingsManager.DB_PATH) || DataBase.Instance.Products.Rows.Count == 0)
                {
                    DialogResult reply = MessageBox.Show(@"Файл базы данных отсутствует или пуст. Cоздать?", @"Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (reply == DialogResult.Yes)
                    {
                        if (UpdatesEvents != null)
                        {
                            UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.InProgress, "Получение данных о доступных продуктах"));
                            switch (DataManager.GetData(DataManager.ProcessingType.FillProducts))
                            {
                                case ProcessingResult.Complete:
                                    ProductsSelectionForm selForm = new ProductsSelectionForm();
                                    selForm.ShowDialog();
                                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.IsCompleted, "Сохранение базы данных"));
                                    DataBase.SaveDataBase();
                                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.IsCompleted, "Завершено"));
                                    return true;
                                case ProcessingResult.Error:
                                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.IsCompleted, "Ошибка получения данных"));
                                    return false;
                                case ProcessingResult.Stop:
                                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.IsCompleted, "Получение данных прервано"));
                                    break;
                            }
                        }
                        DataBase.SaveDataBase();
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Error in CheckFileDirectoryStructure " + ex.StackTrace);
                if (UpdatesEvents != null)
                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.Error, "Ошибка при получении данных"));
                return false;
            }
        }

        private void OnWaitingMode()
        {
            try
            {
                _timer.Stop();

                DateTime nextCheckTime;
                double interval;

                Helpers.GetNextTimeForCheck(out nextCheckTime, out interval);

                if (UpdatesEvents != null)
                {
                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.AddiditionInfo,
                        string.Format("Последняя проверка {0}", DateTime.Now)));

                    if (nextCheckTime == DateTime.MinValue)
                    {
                        return;
                    }

                    OnRaiseCustomEvent(new CustomEventArgs(string.Format("Следующая проверка в {0}", nextCheckTime),
                        MessageType.Warning));

                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.IsCompleted,
                        string.Format("Следующая проверка в {0}", nextCheckTime)));
                }


                _timer.Interval = interval;

                _timer.Start();
            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog("Ошибка в режиме ожидания: " + ex.Message + " " + ex.StackTrace);
                if (UpdatesEvents != null)
                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.Error,
                        "Ошибка в режиме \'ожидание\'"));
            }
        }

        private void TimerEvent(object sender, ElapsedEventArgs e)
        {
            if (State == GlobalStates.Waiting)
                BeginChecking();
        }

        private void SetState(GlobalStates state)
        {
            State = state;

            switch (state)
            {
                case GlobalStates.Loading:
                    break;
                case GlobalStates.Busy:
                    break;
                case GlobalStates.InProcess:
                    _timer.Stop();
                    break;
                case GlobalStates.Closing:
                    break;
                case GlobalStates.Stoping:
                    _timer.Stop();
                    _dataManager.IsStopped = true;
                    break;
                case GlobalStates.Waiting:
                    OnWaitingMode();
                    break;
            }

        }

        public void ProccessHistoryChanges()
        {
            try
            {

                if (DataBase.Instance.History.GetChanges(DataRowState.Added) == null)
                    return;

                if (UpdatesEvents != null)
                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.AddiditionInfo, "Синхронизация БД"));

                if (Settings.Default.UsingEmailNotifying)
                {
                    BuildAndSendEmailNotify();
                }

                if (Settings.Default.DownloadUpdates)
                {
                    DownloadAllNewUpdates();
                }

                DataBase.Instance.Dataset.AcceptChanges();

            }
            catch (Exception ex)
            {
                LogWriter.Instance.WriteToLog(ex.Message + "::::::" + ex.Source + "::::::" + ex.StackTrace + ":::::::" + ex.TargetSite);
            }

        }

        private void DownloadAllNewUpdates()
        {
            var dataTable = DataBase.Instance.Dataset.Tables["History"].GetChanges(DataRowState.Added);

            if (dataTable != null)
                foreach (DataRow historyRow in dataTable.Rows)
                {
                    DataBase.Instance.History.Rows.Find(new object[] { historyRow["ID"].ToString(), historyRow["ChangeDisc"].ToString(), historyRow["EventVersion"].ToString(), historyRow["EventDate"].ToString() });
                    switch ((DataBase.ProductUpdateEvent)historyRow["ChangeDisc"])
                    {
                        case DataBase.ProductUpdateEvent.Plan:
                            break;
                        case DataBase.ProductUpdateEvent.Release:
                            //       EventType = "Новая версия: ";
                            break;
                        case DataBase.ProductUpdateEvent.ProductContent | DataBase.ProductUpdateEvent.VersionContent:
                            //       EventType = "Внешние файлы: ";
                            break;
                    }
                }
        }

        private void BuildAndSendEmailNotify()
        {
            var emailbody = new StringBuilder();

            var dataTable = DataBase.Instance.Dataset.Tables["History"].GetChanges(DataRowState.Added);

            if (dataTable != null)
                foreach (DataRow historyRow in dataTable.Rows)
                {
                    DataRow foundHistory = DataBase.Instance.History.Rows.Find(new object[] { historyRow["ID"].ToString(), historyRow["ChangeDisc"].ToString(), historyRow["EventVersion"].ToString(), historyRow["EventDate"].ToString() });

                    string eventType = String.Empty;

                    switch ((DataBase.ProductUpdateEvent)historyRow["ChangeDisc"])
                    {
                        case DataBase.ProductUpdateEvent.Plan:
                            eventType = "Планируется: ";
                            break;
                        case DataBase.ProductUpdateEvent.Release:
                            eventType = "Новая версия: ";
                            break;
                        case DataBase.ProductUpdateEvent.ProductContent | DataBase.ProductUpdateEvent.VersionContent:
                            eventType = "Внешние файлы: ";
                            break;
                    }

                    emailbody.AppendFormat("{0}. {1} {2} ({3})\r\n", historyRow["Name"], eventType, historyRow["EventVersion"], historyRow["EventDate"]);
                }

            if (Settings.Default.UsingEmailNotifying)
            {
                if (UpdatesEvents != null)
                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.AddiditionInfo, "Отправка e-mail уведомлений"));

                var es = new EmailSender(Settings.Default.SMTPServer,
               int.Parse(Settings.Default.Port),
               Settings.Default.EMailFrom,
               Helpers.Decrypt(Settings.Default.EMailPass, true),
               Settings.Default.EMailLogin,
               Settings.Default.EnableSSL);

                es.SendMail(string.Format("Уведомление об обновлениях 1с от {0}", DateTime.Now), emailbody.ToString(), Settings.Default.EMails);
            }
        }

        public void Stop()
        {
            SetState(GlobalStates.Stoping);
        }

        internal void StartChecking()
        {
            var thread = new Thread(Begin) { IsBackground = true };
            thread.Start();
        }

        private void Begin(object o)
        {
            if (!ConnectionManager.Instance.SetConnection())
            {
                if (UpdatesEvents != null)
                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.Error, "Ошибка проверки обновлений"));
                return;
            }

            if (!CheckFileDirectoryStructure())
            {
                if (UpdatesEvents != null)
                    UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.Error, "База данных пуста"));
                return;
            }

            BeginChecking();
        }

        private void BeginChecking()
        {
            SetState(GlobalStates.InProcess);

            //Проверка обновлений
            if (UpdatesEvents != null)
            {
                UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.InProgress, "Выполняется проверка обновлений"));
                ProcessingResult result = _dataManager.GetData(DataManager.ProcessingType.CheckUpdates);
                UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.IsCompleted, "Проверка обновлений завершена"));

                switch (result)
                {
                    case ProcessingResult.Complete:
                        UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.InProgress, "Обработка обновлений"));
                        ProccessHistoryChanges();
                        UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.IsCompleted, "Обработка обновлений завершена"));
                        SetState(GlobalStates.Waiting);
                        DataBase.SaveDataBase();
                        break;

                    case ProcessingResult.Error:
                        UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.Error, "Ошибка проверки обновлений"));
                        SetState(GlobalStates.Stoping);
                        break;

                    case ProcessingResult.Stop:
                        UpdatesEvents(this, new UpdatesEventHandler(UpdatesStatus.Error, "Проверка прервана"));
                        SetState(GlobalStates.Stoping);
                        break;
                }
            }
        }
    }
}
