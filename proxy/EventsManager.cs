﻿using System;

namespace proxy
{
    public class CustomEventArgs : EventArgs
    {
        public string Message { get; set; }
        public MessageType MessangeType { get; set; }

        public CustomEventArgs(string s, MessageType t)
        {
            Message = t.ToString().ToUpper() + ": " + s;
            MessangeType = t;
        }
    }
}
