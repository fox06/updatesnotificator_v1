﻿using System;

namespace proxy
{
    class CurrentProcEventHandler : EventArgs
    {
        private string _currentProcObj;

        public string CurrentProcObj
        {
            get { return _currentProcObj; }
            set { _currentProcObj = value; }
        }

        public CurrentProcEventHandler(string curProcObj)
        {
            _currentProcObj = curProcObj;
        }
    }
}
