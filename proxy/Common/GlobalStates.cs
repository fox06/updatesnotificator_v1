namespace proxy.Common
{
    internal enum GlobalStates
    {
        Loading,
        Busy,
        InProcess,
        Closing,
        Stoping,
        Waiting
    }
}