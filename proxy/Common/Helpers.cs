﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using HtmlAgilityPack;
using Microsoft.Win32;
using proxy.Configs;
using proxy.Connection;
using proxy.Download;
using proxy.Logger;
using proxy.Properties;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace proxy.Common
{
    static class Helpers
    {


        public static bool SetAutorunRegValue(bool autorun)
        {
            string name = Application.ProductName;
            string exePath = SettingsManager.EXE_PATH;
            RegistryKey reg;
            reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\");
            try
            {
                if (autorun)
                    reg.SetValue(name, exePath + " /a");
                else
                    reg.DeleteValue(name);

                reg.Close();
            }
            catch
            {                
                return false;
            }
            return true;
        }

        public static string GetUpdatesFolder()
        {
            if (Settings.Default.UseDefaultUpdFolder)
                return SettingsManager.DEFAULT_UPD_FOLDER;
            
            return Settings.Default.UpdatesFolder;
        }

        public static DateTime TimeFromStringToDate(string time)
        {        
            DateTime k = new DateTime();
            DateTime.TryParse(time, out k);

            return k;
        }

        public static void GetNextTimeForCheck(out DateTime NextDate, out double mSecInterval)
        {
            NextDate = GetNextDateOfCheck();

            if (NextDate == DateTime.MinValue)
            {
                NextDate = DateTime.MinValue;
                mSecInterval = 0;
                return;
            }

            DateTime now = DateTime.Now;
            DateTime from = new DateTime(now.Year, now.Month, now.Day, Settings.Default.TimeFrom.Hour, Settings.Default.TimeFrom.Minute, 0);
            DateTime _to = new DateTime(now.Year, now.Month, now.Day, Settings.Default.TimeTo.Hour, Settings.Default.TimeTo.Minute, 0);
                      
            TimeSpan ts = new TimeSpan();
            


            if (NextDate.Date == DateTime.Today)
            {
                if (now < from || now.AddMinutes(Settings.Default.CheckUpdatesInterval) < from)
                {
                    ts = from - now;
                    NextDate = DateTime.Now.AddMinutes(ts.TotalMinutes);
                    mSecInterval = ts.TotalMilliseconds;
                }
                else if (now > _to && now.AddMinutes(Settings.Default.CheckUpdatesInterval) > _to)
                {
                    ts = (from.AddDays(1)) - now;
                    NextDate = DateTime.Now.AddMinutes(ts.TotalMinutes);
                    mSecInterval = ts.TotalMilliseconds;
                }
                else if (now < _to && now.AddMinutes(Settings.Default.CheckUpdatesInterval) > _to)
                {
                    ts = _to - now;
                    NextDate = DateTime.Now.AddMinutes(ts.TotalMinutes);
                    mSecInterval = ts.TotalMilliseconds;
                }
                else
                {
                    ts = new TimeSpan(0, Settings.Default.CheckUpdatesInterval, 0);
                    NextDate = DateTime.Now.AddMinutes(ts.TotalMinutes);
                    mSecInterval = ts.TotalMilliseconds;
                }
            }
            else
            {
               // ts = new TimeSpan(
                NextDate = new DateTime(NextDate.Year, NextDate.Month, NextDate.Day, from.Hour, from.Minute, 0);
                ts = NextDate - now;
                mSecInterval = ts.TotalMilliseconds;
            }

         
        }

        public static DateTime GetNextDateOfCheck()
        {
           // DateTime current = DateTime.Today.;
            DateTime current = DateTime.Now.AddMinutes(Settings.Default.CheckUpdatesInterval);
            int _day = (int)current.DayOfWeek == 0 ? 7 : (int)current.DayOfWeek;

            if (Settings.Default.DaysForCheck[_day - 1] == "true")
                return current;


            for (int i = 0; i < 7; i++)
            {
                int _cur = (int)current.DayOfWeek == 0 ? 7 : (int)current.DayOfWeek;

                if (_cur + i < 7)
                {
                    if (Settings.Default.DaysForCheck[_cur + i] == "true")
                    {
                        return current.AddDays(i);
                    }
                }
                else
                {
                    if (Settings.Default.DaysForCheck[_cur - (7 - i)] == "true")
                    {
                        return current.AddDays(i);
                    }
                }

            }

            //for (int i = (int)current.DayOfWeek - 1; i < Properties.Settings.Default.DaysForCheck.Count; i++)
            //{
            //    if (Properties.Settings.Default.DaysForCheck[i] == "true")
            //    {
            //        return current.AddDays(i - (int)current.DayOfWeek + 1);
            //    }
            //}

            //for (int i = (int)current.DayOfWeek - 1; i >= 0; i--)
            //{
            //    if (Properties.Settings.Default.DaysForCheck[i] == "true")
            //    {
            //        return current.AddDays(i - (int)current.DayOfWeek + 8);
            //    }
            //}

            return DateTime.MinValue;


        }

        public static string GetFileName(string fileHref, string fileFolder, bool rewrite = false)
        {
            string fileName = Path.GetFileName(fileHref);
            string fullPath = Path.Combine(GetUpdatesFolder(), fileFolder);

            if (!Directory.Exists(fullPath))
                Directory.CreateDirectory(fullPath);

            if (rewrite == false)
            {
                for (int i = 0; i < 100; i++)
                {
                    if (File.Exists(fullPath + fileName))
                    {
                        fileName = Path.GetFileNameWithoutExtension(fileHref) + "[" + i + "]" + Path.GetExtension(fileHref);
                    }
                    else
                        break;
                }
            }

            return fileName;
        }

        public static List<LinkFileInfo> GetFilesDiscFromHttp(Uri link)
        {

            WebResponse webResponse = ConnectionManager.Instance.GetWebResponse(link, null);

            List<LinkFileInfo> FilesList = new List<LinkFileInfo>();
            GetWebFilesDiscription(webResponse, FilesList);

            webResponse.Close();

            return FilesList;

        }

        private static void GetWebFilesDiscription(WebResponse webResponse, List<LinkFileInfo> files)
        {
            string result = ConnectionManager.Instance.GetResponseString(webResponse, Encoding.UTF8);

            foreach (var it in HtmlParser.GetHrefsWithPattern(result, ".rar", ".zip", ".exe", ".htm", ".html"))
            {
                Uri requestHttp;

                if (it.Key.Contains("http://"))
                    requestHttp = new Uri(it.Key);
                else
                    requestHttp = new Uri(SettingsManager.HREF_V8 + it.Key);

                HttpWebRequest _WebRequest1 = (HttpWebRequest)WebRequest.Create(requestHttp);
                _WebRequest1.Headers.Add("Cookie", SettingsManager.JSONCODE);

                try
                {
                    webResponse = (HttpWebResponse)_WebRequest1.GetResponse();
                }
                catch (Exception)
                {
                    LogWriter.Instance.WriteToLog(requestHttp + " not found", LogLevel.Error);
                    continue;
                }

                if (webResponse.ResponseUri.ToString().Contains("getfile") || webResponse.ResponseUri.ToString().Contains("downloads"))
                {
                    files.Add(new LinkFileInfo(webResponse.ResponseUri.AbsoluteUri,
                        webResponse.ResponseUri.Segments[webResponse.ResponseUri.Segments.Length - 1],
                        webResponse.ContentLength,
                        it.Value));
                }
                else if (webResponse.ResponseUri.ToString().Contains("getdist") || webResponse.ResponseUri.ToString().Contains(".exe") || webResponse.ResponseUri.ToString().Contains(".rar"))
                {
                    GetWebFilesDiscription(webResponse, files);
                }


            }
        }

        public static List<LinkInfo> GetLinksWithPattern(Uri link, params string[] pattern)
        {
            WebResponse webResponse = ConnectionManager.Instance.GetWebResponse(link, ConnectionManager.Instance.CookieContainer.GetCookieHeader(ConnectionManager.Instance.CookieHostname));

            string result = ConnectionManager.Instance.GetResponseString(webResponse, Encoding.GetEncoding("windows-1251"));


            List<LinkInfo> linksInfo = new List<LinkInfo>();
            foreach (var it in HtmlParser.GetHrefsWithPattern(result, pattern))
            {
                linksInfo.Add(new LinkInfo(it.Key, it.Value));
            }

            return linksInfo;
        }

        public static List<ProductInfo> GetProductsInfoFromMainPage(string html, string patternString)
        {


            WebResponse webResp = ConnectionManager.Instance.GetWebResponse(new Uri(html), null);
            string result = ConnectionManager.Instance.GetResponseString(webResp, Encoding.UTF8);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(result);

            List<ProductInfo> foundList = new List<ProductInfo>();

            //Поиск всех нодов HREF содержащих ссылки на страницы конфигураций
            foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
            {
                HtmlAttribute attrib = link.Attributes["href"];

                if (!foundList.Any(p => p.ProductHref == attrib.Value.Trim()))
                    if (attrib.Value.Trim().Contains(patternString))
                    {
                        //Страница продуктов представляет из себя таблицу с 8 колонками
                        //1 - наименование, 2 - Номер актуальной версии, 3 - дата актуальной версии
                        //4 - Планируемая версия 5 - дата выхода планируемой версии итд

                        //Получение ссылки на строку таблицы текущего значения
                        HtmlNodeCollection tableRow = link.ParentNode.ParentNode.SelectNodes(".//td");

                        //if (SettingsManager.ReconfigMode.Length > 0)
                     //   {
                          //  string f = Helpers.Encrypt(HTMLParser.GetIDFromHref(HTMLParser.ClearString(attrib.Value)), true);
                       //     string res = Array.Find<string>(SettingsManager.ReconfigMode, element => element.Contains(f));

                        //    if (res == null)
                        //    {
                       //         continue;
                        //    }
                            
                      //      Console.WriteLine(res);

                            //if (Array.Find<string>(SettingsManager.ReconfigMode, element => element.StartsWith(, StringComparison.Ordinal)) == String.Empty)
                            //{
                            //    continue;
                            //}
                     //   }
                        

                        foundList.Add(new ProductInfo
                        {
                            ProductHref = HtmlParser.ClearString(attrib.Value),
                            ProductId = HtmlParser.GetIdFromHref(HtmlParser.ClearString(attrib.Value)),
                            ProductName = HtmlParser.ClearString(tableRow[0].InnerText),
                            Version = HtmlParser.ClearString(tableRow[1].InnerText),
                            VersionDate = HtmlParser.ClearString(tableRow[2].InnerText),
                            PlanVersion = HtmlParser.ClearString(tableRow[3].InnerText),
                            PlanDate = HtmlParser.ClearString(tableRow[4].InnerText),
                            ReviewVersion = HtmlParser.ClearString(tableRow[6].InnerText),
                            ReviewDate = HtmlParser.ClearString(tableRow[7].InnerText),
                            ReviewHref = HtmlParser.ClearString(HtmlParser.GetHrefFrom(tableRow[6].InnerHtml)),
                            VersionHref = HtmlParser.ClearString(HtmlParser.GetHrefFrom(tableRow[1].InnerHtml))
                        });

                    }
            }

            doc = null;

            return foundList;
        }


        public static string Encrypt(string toEncrypt, bool useHashing, string key = "")
        {
            byte[] keyArray;
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);

            AppSettingsReader settingsReader = new AppSettingsReader();

      
            if (key == "")
            {
                key = "K7jah16Fahs33"; 
            }            
                        
            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key2
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);

        }

        public static string Decrypt(string cipherString, bool useHashing, string key = "")
        {
            if (cipherString == String.Empty)
            {
                return cipherString;
            }

            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);


            //Get your key from config file to open the lock!
            if (key == "")
            {
                key = "K7jah16Fahs33";
            }

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return Encoding.UTF8.GetString(resultArray);
        }

        public static string GetAssembly()
        {
            string ver = String.Empty;

            object[] attr = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyConfigurationAttribute), false);
            if (attr.Length > 0)
            {
                AssemblyConfigurationAttribute aca = (AssemblyConfigurationAttribute)attr[0];
                ver = aca.Configuration;
            }

            return ver;
        }
        public static bool OpenFolder(string path)
        {
            if (Directory.Exists(path))
            {
                Process prc = new Process();
                prc.StartInfo.FileName = path;
                prc.Start();
                return true;
            }

            return false;            
        }
    }
}
