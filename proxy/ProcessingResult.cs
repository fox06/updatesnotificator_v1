namespace proxy
{
    public enum ProcessingResult
    {
        Complete,
        Error,
        Stop
    }
}